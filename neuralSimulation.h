/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INSECT_DEMO_H
#define INSECT_DEMO_H

#ifdef _WINDOWS
#include "Win32DemoApplication.h"
#define PlatformDemoApplication Win32DemoApplication
#else
#include "GlutDemoApplication.h"
#endif

#include <string>
#include <iostream>
#include "LinearMath/btAlignedObjectArray.h"
#include "simulation/terrainFlat.h"
#include "simulation/character.h"
#include "utility/JsonUtil.h"

class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;



class InsectDemo : public GlutDemoApplication
{

	//keep the collision shapes, for deletion/cleanup
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;
	btBroadphaseInterface*	m_broadphase;
	btCollisionDispatcher*	m_dispatcher;
	btConstraintSolver*	m_solver;
	btDefaultCollisionConfiguration* m_collisionConfiguration;
    Terrain*    terrain_;
    Character*  charac_;
    std::string charac_mode_;
public:
    float m_Time;

	InsectDemo();
	virtual ~InsectDemo();


	bool    LoadArgs(char* argv, Json::Value& args);
	void	initPhysics();
	void    initTerrain();
	void    initCharacter(const Json::Value& args);
	void	exitPhysics();

	virtual void clientMoveAndDisplay();
	virtual void displayCallback();
	virtual void clientResetScene();

	void resetCameraPosition(btVector3 cameraPos);
	void draw_unit_coordinate();
	void setMotorTargets(btScalar timeStep);

    void UpdateSimulationStep(double timeStep);
    void SwitchKinematicToDynamic();
    void SwitchDynamicToKinematic();


    virtual void keyboardCallback(unsigned char key, int x, int y);

};

#endif //INSECT_DEMO_H

