#ifndef SENSOR_H
#define SENSOR_H

#include "Eigen/Dense"
#include "Eigen/StdVector"

#include "simulation/skeleton.h"

class Sensor
{
public:
    Sensor();
    ~Sensor();
private:
    int num_sensor_;
    Skeleton* skeleton_;
    Eigen::VectorXd sensory_input_;
};

#endif // SENSOR_H
