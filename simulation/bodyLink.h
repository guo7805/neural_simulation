#ifndef BODYLINK_H
#define BODYLINK_H

#include "btBulletDynamicsCommon.h"
#include "simulation/bodyJoint.h"
#include "simulation/posture.h"

#include "utility/JsonUtil.h"

class BodyJoint;

class BodyLink : public btRigidBody
{

public:
    struct BodyLinkInfo
    {
        int link_id_;
        double mass;
        BodyJoint* parent_joint_;
        BodyJoint* child_joint_;
    };

    BodyLink(const btRigidBodyConstructionInfo &constructionInfo);
    BodyLink(const btRigidBodyConstructionInfo &constructionInfo, BodyLinkInfo link_info);

    ~BodyLink();
    BodyLink* RebuildDynamics();

    int GetLinkID() const {return link_info_.link_id_;};
    BodyJoint* GetParentJoint() const {return link_info_.parent_joint_;};
    BodyJoint* GetChildJoint() const  {return link_info_.child_joint_;};

    void SetParentJoint(BodyJoint* pJ){link_info_.parent_joint_ = pJ;};
    void SetChildJoint(BodyJoint* cJ) {link_info_.child_joint_ = cJ;};

    btTransform ForwardKinematics(Posture* p);
    Eigen::VectorXd PositionOfLink(Posture*p);
private:
    BodyLinkInfo link_info_;
};


#endif // BODYLINK_H
