#ifndef SENSORFORCE_H
#define SENSORFORCE_H

#include "Eigen/Dense"
#include "Eigen/StdVector"

#include "utility/JsonUtil.h"


class SensorForce
{
public:
    SensorForce();
    ~SensorForce();
    SensorForce(std::string filename);
    Eigen::VectorXd GetForce(int force_ind);
private:
    int num_forces_;
    std::vector<Eigen::VectorXd> force_vector_;
};


#endif // SENSORFORCE_H
