#include "simulation/controllerNNPD.h"

ControllerNNPD::ControllerNNPD() : ControllerNN()
{

}

ControllerNNPD::~ControllerNNPD()
{

}

void ControllerNNPD::LoadNet(std::string nn_file)
{
    ControllerNN::LoadNet(nn_file);
}


void ControllerNNPD::NewState()
{
    ControllerNN::NewState();

}

//Eigen::VectorXd ControllerNNPD::GetCtrlState()
//{
//    Eigen::VectorXd state = ControllerNN::GetCtrlState();
//    return state;
//}

void ControllerNNPD::NewCycle(Eigen::VectorXd input_state)
{
    Controller::NewCycle();
    Eigen::VectorXd action = ControllerNN::DecideAction(input_state);
    std::vector<Eigen::VectorXd> action_vector = SegmentIntoVector(action);
    SetControlParamsVector(action_vector);
    //SetControlParamsVector(default_control_params_vector);
}

std::vector<Eigen::VectorXd> ControllerNNPD::SegmentIntoVector(Eigen::VectorXd action)
{
    int num_states = Controller::GetNumStates();
    int num_dim = action.rows();
    int num_dim_per_state = num_dim/num_states;
    std::vector<Eigen::VectorXd> action_vector;
    for(int inds=0; inds<num_states; inds++)
    {
        //Eigen::VectorXd action_per_state;
        Eigen::Map<Eigen::VectorXd> action_per_state(action.data()+num_dim_per_state*inds, num_dim_per_state);
        action_vector.push_back(action_per_state);
    }
    return action_vector;
}



void ControllerNNPD::LoadController(std::string control_file_name)
{
    bool succ;
    Json::Value ctrl_info;
    succ = JsonUtil::LoadFileIntoJsonValue(control_file_name, ctrl_info);

    if (succ)
    {
        ParseStateDuration(ctrl_info);
        ParseControlParams(ctrl_info);
    }
    else
    {
        std::cout << "Failed to load " << control_file_name << std::endl;
    }

}

void ControllerNNPD::ParseControlParams(Json::Value ctrl_info)
{
    int num_states = ctrl_info.size();

    for(int inds=0; inds<num_states; inds++)
    {
        Eigen::VectorXd ctrl_params = ParseControlParamsPerState(ctrl_info[inds]["targetAngle"]);
        default_control_params_vector.push_back(ctrl_params);
        control_params_vector.push_back(ctrl_params);
    }
}

Eigen::VectorXd  ControllerNNPD::ParseControlParamsPerState(Json::Value ctrl_info_per_state)
{
    int num_joints = ctrl_info_per_state.size();
    Eigen::VectorXd ctrl_params(num_joints);
    for(int indj=0; indj<num_joints; indj++)
    {
        double param = ctrl_info_per_state[indj].asFloat();
        ctrl_params[indj] = param;
    }
    return ctrl_params;
}


std::vector<Eigen::VectorXd> ControllerNNPD::GetControlParamsVector()
{
    return control_params_vector;
}

void ControllerNNPD::SetControlParamsVector(std::vector<Eigen::VectorXd> control_params_vector_in)
{
    control_params_vector = control_params_vector_in;
}

Eigen::VectorXd ControllerNNPD::GetControlParams(int ind)
{
    int clean_ind = Controller::CleanState(ind);
    return control_params_vector[clean_ind];
}

void ControllerNNPD::SetControlParams(Eigen::VectorXd control_params_in)
{
    control_params = control_params_in;
}


Eigen::VectorXd ControllerNNPD::GetBlendControlParams()
{
    int state = Controller::GetState();
    double phase = Controller::GetPhase();
    Eigen::VectorXd control_params_pre = GetControlParams(state-1);
    Eigen::VectorXd control_params_cur = GetControlParams(state);
    Eigen::VectorXd control_params_blend = (1 - phase) * control_params_pre + phase * control_params_cur;
    return control_params_blend;
}
