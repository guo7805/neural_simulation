#ifndef CONTROLLERNNPDHEXAPOD_H
#define CONTROLLERNNPDHEXAPOD_H

#include "simulation/controllerNNPD.h"

class ControllerNNPDHexapod : public ControllerNNPD
{
public:
    ControllerNNPDHexapod();
    ~ControllerNNPDHexapod();

    virtual void SetControlState(Eigen::VectorXd inputs);
    virtual Eigen::VectorXd GetControlState();
    virtual Eigen::VectorXd InitControlState();
    //virtual void NewCycle(Eigen::VectorXd control_state);
    //virtual void NewCycle(Eigen::VectorXd initial);
    //virtual void NewCycle(Eigen::VectorXd initial, Eigen::VectorXd target);
private:
    Eigen::VectorXd control_state_;
};


#endif // CONTROLLERNNPDHEXAPOD_H
