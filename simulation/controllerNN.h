#ifndef CONTROLLERNN_H
#define CONTROLLERNN_H

#include "Eigen/Dense"
#include "Eigen/StdVector"

#include "simulation/controller.h"
#include "simulation/sensorPosture.h"
#include "learning/neuralNet.h"


// This class implements the Neural Network Controller
// Inherites from the base class Controller, with the related functions dealing the states and phases
// This NN controller is called once for each step, and sets all the control parameters for all states in a step
class ControllerNN : public Controller
{
public:
    // Initialization Part
    ControllerNN();
    virtual ~ControllerNN();
	virtual void Reset();
	virtual void Clear();
	virtual void Update(double time_step);
    virtual void LoadNet(const std::string& net_file);
    virtual void LoadNet(NeuralNet* net);
	virtual void LoadModel(const std::string& model_file);
	virtual void LoadScale(const std::string& scale_file);

	//virtual void SetTarget(Eigen::VectorXd target);
	//virtual void SetInitial(Eigen::VectorXd initial);
	//virtual Eigen::VectorXd DecideTargetPosition();

    //virtual Eigen::VectorXd GetTarget() {return target_;};
    //virtual Eigen::VectorXd GetInitial() {return initial_;};

    // Evaluation Part
    //virtual Eigen::VectorXd GetCtrlState();
    //virtual Eigen::VectorXd IntegrateInput(Eigen::VectorXd external_input);
    virtual Eigen::VectorXd DecideAction(Eigen::VectorXd input_state);

    // Utility Part
    virtual void NewState();
    virtual int GetNetInputSize() const;
	virtual int GetNetOutputSize() const;

private:
    // Customized NeuralNet class to manipulate the neural network from Caffe
	NeuralNet* mNet_;
    //Eigen::VectorXd target_;
    //Eigen::VectorXd initial_;
};


#endif // CONTROLLERNN_H
