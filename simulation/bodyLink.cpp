#include "simulation/bodyLink.h"
#include "utility/MathUtil.h"


BodyLink::BodyLink(const btRigidBodyConstructionInfo &constructionInfo) : btRigidBody(constructionInfo)
{

}


BodyLink::~BodyLink()
{

}

BodyLink::BodyLink(const btRigidBodyConstructionInfo &constructionInfo, BodyLinkInfo link_info) : btRigidBody(constructionInfo)
{
    link_info_ = link_info;
}


BodyLink* BodyLink::RebuildDynamics()
{
    btTransform tr = getCenterOfMassTransform();
    btCollisionShape * col_shape = getCollisionShape();
    btVector3 inertia;
    col_shape->calculateLocalInertia(link_info_.mass, inertia);
    btDefaultMotionState *motionState = new btDefaultMotionState(tr);
    btRigidBodyConstructionInfo cinfo(link_info_.mass, motionState, col_shape, inertia);
    BodyLinkInfo link_info = link_info_;
    link_info.parent_joint_ = nullptr;
    link_info.child_joint_ = nullptr;
    BodyLink* bl = new BodyLink(cinfo, link_info);
    return bl;
}


btTransform BodyLink::ForwardKinematics(Posture* p)
{
    btTransform tr;
    tr.setIdentity();
    BodyJoint* parent_joint = GetParentJoint();
    if(parent_joint!=nullptr)
    {
        // if current link is not the main body
        btTransform trA_local, trB_local, tr_joint, tr_parent;

        trA_local = parent_joint->getAFrame();
        double angle = p->GetJointData(parent_joint->GetJointID());
        tr_joint = parent_joint->GetRotationTransform(angle);
        trB_local = parent_joint->getBFrame();
        BodyLink* parent_link = parent_joint->GetParentLink();
        tr_parent = parent_link->ForwardKinematics(p);
        tr = tr_parent*trA_local*tr_joint*trB_local.inverse();
    }
    else
    {
        // if current link is the main body
        tr = getCenterOfMassTransform ();
    }
    return tr;
}


Eigen::VectorXd BodyLink::PositionOfLink(Posture*p)
{
    btTransform tr = ForwardKinematics(p);
    btVector3 tr_pos = tr.getOrigin();
    Eigen::VectorXd pos = MathUtil::ConvertBulletToEigen(tr_pos);
    return pos;
}
