#include "simulation/terrainFlat.h"

TerrainFlat::TerrainFlat()
{
    btVector3 terrain_size(50.0, 50.0, 50.0);
    btVector3 terrain_pos(0,-50,0);
    TerrainFlat(terrain_size, terrain_pos);
}

TerrainFlat::TerrainFlat(btVector3 terrain_size, btVector3 terrain_pos)
{
    terrainShape = new btBoxShape(terrain_size);

    terrainTransform.setIdentity();
    terrainTransform.setOrigin(terrain_pos);

    //We can also use DemoApplication::localCreateRigidBody, but for clarity it is provided here:

    btScalar mass(0.);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0,0,0);
    if (isDynamic)
        terrainShape->calculateLocalInertia(mass,localInertia);

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(terrainTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,terrainShape,localInertia);
    terrainBody = new btRigidBody(rbInfo);
}


TerrainFlat::~TerrainFlat()
{

}

btBoxShape* TerrainFlat::get_terrain_shape()
{
    return terrainShape;
}

btRigidBody* TerrainFlat::get_terrain_body()
{
    return terrainBody;
}
