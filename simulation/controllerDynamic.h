#ifndef CONTROLLERDYNAMIC_H
#define CONTROLLERDYNAMIC_H

#include "simulation/controller.h"
#include "simulation/actuatorSys.h"
#include "simulation/sensorSys.h"


class ControllerDynamic : public Controller
{
public:
    ControllerDynamic();
    ~ControllerDynamic();
    ControllerDynamic(Json::Value args,btDynamicsWorld* m_dynamicsWorld);

    virtual void LoadActuatorSys(const std::string& actuator_file)=0;
    virtual void LoadController(const std::string& control_file)=0;
    virtual void Update(double time_step);
    virtual Eigen::VectorXd GetBlendControlParams()=0;

    void InitPosture(std::string posture_file_name);
    //void LoadInitialPosture();
    void LoadSensorSys();

    // Dynamic Simulation
    //void    LoadNeuralNetwork(std::string nn_file_name);
    void    ApplyTorque(double timeStep);
    void    UpdateActuatorControlParams(Eigen::VectorXd control_params);
    void    InitLocomotion();
private:
    ActuatorSys*   actuator_sys_;
    SensorSys*     sensor_sys_;

};


#endif // CONTROLLERDYNAMIC_H
