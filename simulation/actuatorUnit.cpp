#include "simulation/actuatorUnit.h"

ActuatorUnit::ActuatorUnit()
{

}


ActuatorUnit::~ActuatorUnit()
{


}

void ActuatorUnit::Init()
{

}



int ActuatorUnit::GetNumOptParams() const
{
    return 0;
}

void ActuatorUnit::ReadParams(const std::string& file)
{

}

void ActuatorUnit::ReadParams(std::ifstream& f_stream)
{

}

void ActuatorUnit::SetOptParams(const std::vector<double> params)
{


}

void ActuatorUnit::SetEffectiveJoint(btHingeConstraint* pJoint_in)
{
    pJoint = pJoint_in;
    parent = &(pJoint->getRigidBodyA());
    child = &(pJoint->getRigidBodyB());
}

bool ActuatorUnit::HasParent()
{
    return parent != nullptr;
}


btVector3 ActuatorUnit::GetAxisInWorld()
{
    //btVector3 axis_in_world(0.0, 0.0, 1.0);
    btVector3 zAxis(0.0, 0.0, 1.0);
    btTransform trA = pJoint->getAFrame();
    btTransform rbA = parent->getCenterOfMassTransform();
    btVector3 axis_in_world = rbA.getBasis()*trA.getBasis()*zAxis;
    return axis_in_world;
}

void ActuatorUnit::ApplyTorque(btScalar tau)
{
    btVector3 axis_in_world = GetAxisInWorld();
    btVector3 torque = axis_in_world*tau;
    if(HasParent())
    {
        parent->applyTorque(-torque);
    }

    child->applyTorque(torque);
}

double ActuatorUnit::ClampTorque(double torque)
{
    if (torque>torque_limit)
        return torque_limit;
    else if(torque<-torque_limit)
        return -torque_limit;
    return torque;
}
