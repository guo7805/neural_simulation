#ifndef SENSORSYS_H
#define SENSORSYS_H

#include "simulation/sensorPosture.h"

class SensorSys
{
public:
    SensorSys();
    ~SensorSys();
    SensorSys(Skeleton* skel_);
    //SensorSys(Skeleton* skel_, Terrain* terr);

    Eigen::VectorXd GetCOMPosition();
    Eigen::VectorXd GetCOMRotation();
    Eigen::VectorXd GetCurrentState();
    Eigen::VectorXd GetPostureVector();
    Eigen::VectorXd IntegrateSensor();
private:
    SensorPosture* sensor_posture_;

};


#endif // SENSORSYS_H
