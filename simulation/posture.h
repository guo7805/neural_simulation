#ifndef POSTURE_H
#define POSTURE_H


#include "Eigen/Dense"
#include "Eigen/StdVector"

#include "utility/JsonUtil.h"

class Posture
{
public:
    Posture();
    ~Posture();
    Posture(std::string posture_file_name);
    Posture(Json::Value posture_info);
    void ParsePosture(Json::Value posture_info);

    double GetJointData(int jointID);

    void SetPostureData(Eigen::VectorXd pData);
    Eigen::VectorXd GetPostureData() {return posture_data_;};

    double operator[](int joint_id);

private:
    Eigen::VectorXd posture_data_;
};

#endif // POSTURE_H
