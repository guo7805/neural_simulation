#include "simulation/sensorForce.h"


SensorForce::SensorForce()
{

}

SensorForce::~SensorForce()
{

}

SensorForce::SensorForce(std::string file_name)
{
    Json::Value force_info;
    JsonUtil::LoadFileIntoJsonValue(file_name, force_info);
    num_forces_ = force_info.size();

    for(int indf=0; indf<num_forces_; indf++)
    {
        Eigen::VectorXd force = Eigen::VectorXd::Zero(3);
        for(int indd=0; indd<3; indd++)
        {
            double force_scalar = force_info[indf][indd].asFloat();
            force[indd] = force_scalar;
        }
        force_vector_.push_back(force);
    }
}



Eigen::VectorXd SensorForce::GetForce(int force_id)
{
    if(force_id>=num_forces_)
        force_id = num_forces_ - 1;
    return force_vector_[force_id];
}
