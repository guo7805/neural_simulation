#include <iostream>
#include <fstream>
#include "simulation/character.h"
#include "simulation/controllerNNPD.h"
#include "simulation/controllerNNPDHexapod.h"


Character::Character()
{
    skeleton_ = nullptr;
    actuator_sys_ = nullptr;
    sensor_sys_ = nullptr;
    controller_ = nullptr;
    controller_kinematic_ = nullptr;
//    is_kinematic_ = true;
}

Character::Character(std::string skel_file_name)
{

    cLogger = new Logger();

    skeleton_ = new Skeleton;
    skeleton_->LoadSkeleton(skel_file_name);

    actuator_sys_ = nullptr;
    sensor_sys_ = nullptr;
    controller_ = nullptr;
    controller_kinematic_ = nullptr;
}


Character::Character(std::string skel_file_name, std::string actuator_file_name, std::string controller_file_name)
{
    cLogger = new Logger();

    skeleton_ = new Skeleton;
    skeleton_->LoadSkeleton(skel_file_name);

    actuator_sys_ = new ActuatorSys;
    actuator_sys_->LoadActuator(actuator_file_name);

    LinkSkeletonWithActuator();

    sensor_sys_ = new SensorSys(skeleton_);

    //controller_ = new ControllerNNPD;
    controller_ = new ControllerNNPDHexapod;
    controller_->LoadController(controller_file_name);
    controller_->TransitionState(0);
    controller_->InitControlState();
}


void Character::LoadActuatorSys(std::string actuatorSys_file_name)
{
    actuator_sys_ = new ActuatorSys;
    actuator_sys_->LoadActuator(actuatorSys_file_name);
    LinkSkeletonWithActuator();
}

void Character::LoadSensorSys(std::string sensorSys_file_name)
{
    sensor_sys_ = new SensorSys(skeleton_);
}

void Character::LoadController(std::string controller_file_name)
{
    controller_ = new ControllerNNPDHexapod;
    controller_->LoadController(controller_file_name);
    controller_->TransitionState(0);
}

void Character::LoadKinematic(std::string motion_file_name)
{
    controller_kinematic_ = new ControllerKinematic(motion_file_name);
}

Character::~Character()
{
    delete skeleton_;
    delete controller_;
    delete actuator_sys_;
    delete sensor_sys_;
    delete controller_kinematic_;
}

void Character::SetKinematicFlag(bool kFlag)
{
    is_kinematic_ = kFlag;
}

double Character::GetCycleDuration()
{
    double cycle_duration = controller_->GetCycleDuration();
    return cycle_duration;
}

void Character::InitPosture(Posture* p)
{
    assert(is_kinematic_==true);
    skeleton_->SetPosture(p);
}

void Character::ResetInitialPose()
{
    controller_kinematic_->Reset();
    Posture* p = controller_kinematic_->Update(1.0/120.0);
    skeleton_->ResetPositionOfCOM();
    skeleton_->SetPosture(p);
}

bool Character::IsMotionFinished()
{
    bool is_finished = controller_kinematic_->IsMotionFinished();
    return is_finished;
}

void Character::UpdateKinematicState(double time_step)
{
    Posture* p = controller_kinematic_->Update(time_step);
    skeleton_->SetPosture(p);
}

void Character::EnableKinematics(btDynamicsWorld* m_dynamicsWorld)
{
    if(is_kinematic_!=true)
        is_kinematic_ = true;
    skeleton_->EnableKinematics(m_dynamicsWorld);
}

void Character::EnableDynamics(btDynamicsWorld* m_dynamicsWorld)
{
    if(is_kinematic_!=false)
        is_kinematic_ = false;
    skeleton_->EnableDynamics(m_dynamicsWorld);
}

void Character::LoadNetwork(std::string nn_file_name)
{
    controller_->LoadNet(nn_file_name);
}

void Character::LoadNetwork(NeuralNet* net)
{
    controller_->LoadNet(net);
}

void Character::LoadNetworkModel(std::string model_file_name)
{
    controller_->LoadModel(model_file_name);
}

Eigen::VectorXd Character::GetSensoryState()
{
    return sensor_sys_->GetCurrentState();
}

Eigen::VectorXd Character::GetControlState()
{
    return controller_->GetControlState();
}

void Character::LinkSkeletonWithActuator()
{
    std::vector<BodyJoint*> joint_vector = skeleton_->GetJointVector();
    std::vector<ActuatorUnit*> actuator_vector = actuator_sys_->GetActuatorVector();

    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        int indj = actuator_vector[inda]->GetEffectiveJointID();
        actuator_vector[inda]->SetEffectiveJoint(joint_vector[indj]);
    }
}

void Character::ApplyTorque(double timeStep)
{
    std::vector<BodyJoint*> joint_vector = skeleton_->GetJointVector();
    std::vector<ActuatorUnit*> actuator_vector = actuator_sys_->GetActuatorVector();
    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        double torque = actuator_vector[inda]->GetTorque(timeStep);
        actuator_vector[inda]->ApplyTorque(torque);
    }

}

void Character::InitLocomotion()
{
    Eigen::VectorXd input_state = sensor_sys_->IntegrateSensor();
    controller_->NewCycle(input_state);
}

Eigen::VectorXd Character::GetCOMPosition()
{
    Eigen::VectorXd pos = sensor_sys_->GetCOMPosition();
    return pos;
}


bool Character::FallOff()
{
    Eigen::VectorXd pos = sensor_sys_->GetCOMPosition();
    double threshold = 1.0;
    if (pos[1]<threshold)
        return true;
    else
        return false;
}

Eigen::VectorXd Character::IntegrateState()
{
    //Eigen::VectorXd sensory_state = sensor_sys_->GetCurrentState();
    //Eigen::VectorXd control_state = controller_->InitControlState();
    //Eigen::VectorXd input_state(sensory_state.rows()+control_state.rows());
    //input_state << sensory_state, control_state;
    //return input_state;
    Eigen::VectorXd sensory_state = sensor_sys_->IntegrateSensor();
    return sensory_state;
}

Eigen::VectorXd Character::GetPostureVector()
{
    Eigen::VectorXd pose = sensor_sys_->GetPostureVector();
    return pose;
}

void Character::UpdateDynamicState(double timeStep)
{
    // check whether the current locomotion cycle finishes
    if (controller_->CheckCycleTransition())
    {
        // if the current cycle finishes, call the NN controller to update the control parameter
        Eigen::VectorXd input_state = IntegrateState();
        controller_->NewCycle(input_state);
    }
    else
    {
        if (controller_->CheckStateTransition())
        {
            // update the state
            // GetBlendControlParams() will fetch the correct control parameter in the controller class
            controller_->NewState();
        }
    }

    controller_->Update(timeStep);
    // update the actuator target angle with the blend control parameter
    Eigen::VectorXd control_params = controller_->GetBlendControlParams();
    UpdateActuatorControlParams(control_params);
}

void Character::UpdateActuatorControlParams(Eigen::VectorXd control_params)
{
    // Maybe should check whether the dimensions of the control_params match the number of actuators
    // Update the actuator parameter at the level of per simulation step. Other updates are operated on the controller level
    std::vector<ActuatorUnit*> actuator_vector = actuator_sys_->GetActuatorVector();
    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        actuator_vector[inda]->SetControlParams(control_params[inda]);
    }

}
