#include "simulation/sensorPosture.h"


SensorPosture::SensorPosture()
{

}

SensorPosture::~SensorPosture()
{

}

SensorPosture::SensorPosture(Skeleton* skel)
{
    skel_ = skel;
}


void SensorPosture::SetSkeleton(Skeleton* skel)
{
    skel_ = skel;
}

Eigen::VectorXd SensorPosture::GetPostureVector()
{
    Posture* posture = new Posture;
    skel_->GetPosture(posture);
    return posture->GetPostureData();
}

Eigen::VectorXd SensorPosture::PositionOfEndEffectors(bool local)
{

    Eigen::VectorXd start_pos, end_pos;

    Posture* posture = new Posture;
    skel_->GetPosture(posture);
    std::vector<int> end_effectors = skel_->GetEndEffectors();

    Eigen::VectorXd end_pos_vec = Eigen::VectorXd::Zero(end_effectors.size()*3);

    if(local)
        start_pos = skel_->PositionOfCOM(posture);

    for(unsigned int inde=0; inde<end_effectors.size(); inde++)
    {
        end_pos = skel_->PositionOfLink(posture, end_effectors[inde]);
        if(local)
        {
            end_pos = end_pos - start_pos;
        }
        end_pos_vec.segment(inde*3,3) = end_pos;
    }

    return end_pos_vec;
}

Eigen::VectorXd SensorPosture::GetCOMPosition()
{
    Eigen::VectorXd pos = skel_->PositionOfCOM();
    return pos;
}

Eigen::VectorXd SensorPosture::GetCOMRotation()
{
    Eigen::VectorXd rot = skel_->RotationOfCOM();
    return rot;
}

Eigen::VectorXd SensorPosture::GetCOMTransformVector()
{
    Eigen::VectorXd pos = GetCOMPosition();
    Eigen::VectorXd rot = GetCOMRotation();
    Eigen::VectorXd global_tr(pos.size()+rot.size());
    global_tr << pos, rot;
    return global_tr;
}
