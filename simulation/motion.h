#ifndef MOTION_H
#define MOTION_H



#include "simulation/posture.h"


class Motion
{
public:
    Motion();
    Motion(std::string motion_file_name);
    ~Motion();

    std::vector<Posture*> GetMotionData() {return motion_data_;};
    Posture* GetPosture();
    int GetPostureID() {return posture_id_;};
    Posture* PerformNextPosture();

    void SetMotionData(std::vector<Posture*> mData);
    bool IsFinished();
    void Reset();
private:
    int posture_id_;
    int num_postures_;
    std::vector<Posture*> motion_data_;
};


#endif // MOTION_H
