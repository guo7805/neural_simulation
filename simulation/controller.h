#ifndef CONTROLLER_H
#define CONTROLLER_H

#include<vector>
#include "Eigen/Dense"
#include "Eigen/StdVector"
#include "learning/neuralNet.h"
#include "utility/JsonUtil.h"

const double kPhaseTransitionThreshold = 0.98;

// This class implements the basic FSM controller, which is further inherited by ControllerCPG and ControllerNN

class Controller
{
public:
    Controller();
    virtual ~Controller();
	virtual void    Reset();
	virtual void    Clear();
	virtual void    Update(double time_step);
	virtual int     GetState() const;
	virtual double  GetPhase() const;
	virtual int     GetNumStates() const;
	virtual double  GetStateDuration(int state);
	virtual double  GetCycleDuration();
	virtual void    LoadController(std::string ctrl_file);
	virtual void    LoadNet(std::string nn_file)=0;
	virtual void    LoadNet(NeuralNet* net)=0;
	virtual void    LoadModel(const std::string& model_file)=0;
	virtual Eigen::VectorXd  GetBlendControlParams()=0;
	virtual void    ParseStateDuration(Json::Value ctrl_info);

	virtual void    SetPhase(double phase);

	virtual double  CalcNormPhase() const;
	virtual void    AppendStateDurationVector(double phase_val);
	virtual int     CleanState(int state_in);

	virtual void    TransitionState(int state);
	virtual void    TransitionState(int state, double phase);
	virtual void    NewState();
	virtual void    NewCycle();
	virtual void    NewCycle(Eigen::VectorXd current_posture);
    virtual bool    CheckStateTransition();
    virtual bool    CheckCycleTransition();

    virtual Eigen::VectorXd    GetControlState()=0;
    virtual Eigen::VectorXd    InitControlState()=0;
private:
    // state is the id for the current state in FSM
    // phase is the current phase in the current state
    // current_state_duration is the time duration for current state
    // state_duration_vector stores time duration for all states
    int state;
    int num_states;
    double phase;
    double current_state_duration;
    std::vector<double> state_duration_vector;
};


#endif
