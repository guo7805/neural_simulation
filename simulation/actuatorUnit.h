#ifndef ACTUATORUNIT_H
#define ACTUATORUNIT_H

#include "btBulletDynamicsCommon.h"
#include<string>
#include<vector>
#include<iostream>

class ActuatorUnit
{
public:

	enum actuatorType
	{
		pd_servo,
		hill_muscle,
	};

	ActuatorUnit();
	virtual ~ActuatorUnit();
	virtual void Init();
	virtual int     GetNumOptParams() const;
	virtual void    ReadParams(const std::string& file);
	virtual void    ReadParams(std::ifstream& f_stream);
	virtual void    SetOptParams(const std::vector<double> params);
	virtual double  GetTorque(double timeStep)=0;    // pure virtual function to compute the torque
    virtual void    SetControlParams(double param)=0;

    void    ApplyTorque(btScalar tau);
    double    ClampTorque(double torque);
	int     GetEffectiveJointID(){return effective_joint_id;};
	bool    HasParent();
	void    SetEffectiveJointID(int indj){ effective_joint_id = indj;};
    void    SetEffectiveJoint(btHingeConstraint* pJoint);
    btVector3 GetAxisInWorld();

protected:
    int actuator_id;
    int effective_joint_id;
    btHingeConstraint* pJoint;
    btRigidBody* parent;
    btRigidBody* child;
    double torque_limit;
};

#endif // ACTUATORUNIT_H
