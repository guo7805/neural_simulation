#ifndef CONTROLLERNNPD_H
#define CONTROLLERNNPD_H

#include <vector>
#include "simulation/controllerNN.h"


// This implements a class ControllerNNPD, which controls the PD actuators with the Neural Network
class ControllerNNPD : public ControllerNN
{
public:
    ControllerNNPD();
    virtual ~ControllerNNPD();
    virtual void    LoadNet(std::string nn_file);


    virtual void    NewState();
    virtual void    NewCycle(Eigen::VectorXd input_state);
    virtual void    LoadController(std::string control_file_name);
    virtual void    ParseControlParams(Json::Value ctrl_info);
    virtual Eigen::VectorXd    ParseControlParamsPerState(Json::Value ctrl_info_per_state);

    virtual void    SetControlParamsVector(std::vector<Eigen::VectorXd> control_params_vector_in);
    virtual void    SetControlParams(Eigen::VectorXd control_params_in);
    virtual Eigen::VectorXd GetControlParams(int ind);
    virtual Eigen::VectorXd GetBlendControlParams();
    virtual std::vector<Eigen::VectorXd>    GetControlParamsVector();
    virtual std::vector<Eigen::VectorXd>    SegmentIntoVector(Eigen::VectorXd action);

//    virtual Eigen::VectorXd GetCtrlState();
private:
    // control_params_vector stores the control parameters for all states
    std::vector<Eigen::VectorXd> control_params_vector;
    // default vector stores the control variables in the file
    std::vector<Eigen::VectorXd> default_control_params_vector;
    // control_params stores the control parameters for the current state
    Eigen::VectorXd control_params;

};


#endif // CONTROLLERNNPD_H
