#include "simulation/bodyJoint.h"
#include "utility/MathUtil.h"


BodyJoint::BodyJoint(BodyLink &rbA, BodyLink &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB,
                     const btVector3 &axisInA, const btVector3 &axisInB, bool useReferenceFrameA)
    :btHingeConstraint(rbA, rbB, pivotInA, pivotInB, axisInA, axisInB)
{

}


BodyJoint::~BodyJoint()
{

}

BodyJoint::BodyJoint(BodyLink &rbA, BodyLink &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB,
                     const btVector3 &axisInA, const btVector3 &axisInB, BodyJointInfo bjInfo, bool useReferenceFrameA)
    :btHingeConstraint(rbA, rbB, pivotInA, pivotInB, axisInA, axisInB)
{
    joint_info_ = bjInfo;
}


BodyJoint::BodyJoint(BodyLink &rbA, BodyLink &rbB, const btTransform &frameA, const btTransform &frameB, BodyJointInfo bjInfo, bool useReferenceFrameA)
            : btHingeConstraint(rbA, rbB, frameA, frameB, useReferenceFrameA)
{
    joint_info_ = bjInfo;
}

BodyJoint* BodyJoint::RebuildDynamics(std::vector<BodyLink*> link_vector)
{
    BodyJointInfo new_joint_info = joint_info_;
    BodyLink* parent_link = link_vector[joint_info_.parent_link_id_];
    BodyLink* child_link  = link_vector[joint_info_.child_link_id_];
    btTransform localA = getAFrame();
    btTransform localB = getBFrame();
    BodyJoint* newJ = new BodyJoint(*parent_link, *child_link, localA, localB, new_joint_info, true);
    double upper_limit = getUpperLimit();
    double lower_limit = getLowerLimit();
    newJ->setLimit(lower_limit, upper_limit);
    //newJ->setParam(BT_CONSTRAINT_STOP_CFM, 0.0);
    newJ->setDbgDrawSize(btScalar(2.f));
    return newJ;
}

btVector3 BodyJoint::GetOriginalRotationVector()
{
    switch(joint_info_.axis_)
    {
    // Normally there should be a break for each case
    // But since you use return, there is no need
    case ROTATION_AXIS_X:
        return btVector3(1.0, 0.0, 0.0);
    case ROTATION_AXIS_Y:
        return btVector3(0.0, 1.0, 0.0);
    case ROTATION_AXIS_Z:
        return btVector3(0.0, 0.0, 1.0);
    }
}

int BodyJoint::CheckRotationVector(btVector3 vec)
{
    if (vec == btVector3(1.0, 0.0, 0.0))
        return 0;
    else if (vec == btVector3(0.0, 1.0, 0.0))
        return 1;
    else if (vec == btVector3(0.0, 0.0, 1.0))
        return 2;
}

btTransform BodyJoint::GetRotationTransform(double angle)
{
    btTransform tr;
    tr.setIdentity();

    //btVector3 rot_vec = GetOriginalRotationVector();
    btVector3 rot_vec(0.0, 0.0, 1.0);
    btQuaternion qt(rot_vec, angle);

    tr.setRotation(qt);
    return tr;
}

btTransform BodyJoint::GetRotationTransformDiff(double angle)
{
    btTransform tr;
    tr.setIdentity();
    btTransform tr_rot = GetRotationTransform(angle);
    int axis = GetJointAxis();
    btMatrix3x3 out_rot = MathUtil::RotationMatrixDiff(tr_rot.getBasis(), axis);
    tr.setBasis(out_rot);
    return tr;
}

btTransform BodyJoint::ForwardKinematics(Posture* p, bool include_joint_rotation)
{
    BodyLink* parent_link = GetParentLink();
    btTransform tr = parent_link->ForwardKinematics(p);
    btTransform trA_local = getAFrame();
    if (include_joint_rotation)
    {
        int joint_id = GetJointID();
        double angle = (*p)[joint_id];
        btTransform tr_rot = GetRotationTransform(angle);
        tr = tr*trA_local*tr_rot;
    }
    else
        tr = tr*trA_local;
    return tr;
}

BodyJoint* BodyJoint::GetParentJoint()
{
    BodyLink* parent_link = GetParentLink();
    return parent_link->GetParentJoint();   // This could be null
}

BodyJoint* BodyJoint::GetChildJoint()
{
    BodyLink* child_link = GetChildLink();
    return child_link->GetChildJoint();     // This could be null
}
