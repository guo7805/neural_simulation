#include "simulation/controller.h"
#include "utility/MathUtil.h"

Controller::Controller()
{
    state = 0;
    phase = 0.0;
}

Controller::~Controller()
{

}


void Controller::Reset()
{
    state = 0;
    phase = 0.0;
}

void Controller::Clear()
{
    state = 0;
    phase = 0.0;
}

void Controller::Update(double time_step)
{
    phase += time_step/current_state_duration;
}


int Controller::GetState() const
{
    return state;
}

double Controller::GetPhase() const
{
    return phase;
}

void Controller::SetPhase(double phase_in)
{
    phase = phase_in;
}

int Controller::GetNumStates() const
{
    return num_states;
}

double Controller::GetCycleDuration()
{
    double cycle_duration = 0.0;
    for(unsigned int inds=0; inds<state_duration_vector.size(); inds++)
        cycle_duration += state_duration_vector[inds];
    return cycle_duration;
}

void Controller::LoadController(std::string ctrl_file)
{
    bool succ;
    Json::Value ctrl_info;
    succ = JsonUtil::LoadFileIntoJsonValue(ctrl_file, ctrl_info);

    if (succ)
    {
        ParseStateDuration(ctrl_info);
    }
    else
    {
        std::cout << "Failed to load " << ctrl_file << std::endl;
    }
}

void Controller::ParseStateDuration(Json::Value ctrl_info)
{
    num_states = ctrl_info.size();

    //Json::Value ctrl_data = ctrl_info["ctrl_data"];
    for(int inds=0; inds<num_states; inds++)
    {
        double duration = ctrl_info[inds]["duration"].asFloat();
        state_duration_vector.push_back(duration);
    }
}

double Controller::CalcNormPhase() const
{
    //int state = GetState();
	//double phase = GetPhase();
	double phase_c = MathUtil::Clamp(phase, 0.0, 1.0);

	int num_states = GetNumStates();
	double norm_phase = (state + phase_c) / num_states;
	norm_phase = MathUtil::Clamp(norm_phase, 0.0, 1.0);
	return norm_phase;
}

void Controller::AppendStateDurationVector(double phase_val)
{
    state_duration_vector.push_back(phase_val);
}

double Controller::GetStateDuration(int state)
{
    return state_duration_vector[state];
}


int Controller::CleanState(int state_in)
{
    int clean_state=state_in;
    if(state_in<0)
        clean_state = num_states - 1;
    if(state_in>=num_states)
        clean_state = 0;
    return clean_state;
}

void Controller::TransitionState(int state_in)
{
    TransitionState(state_in, 0.0);
    current_state_duration = state_duration_vector[state_in];
}

void Controller::TransitionState(int state_in, double phase_in)
{
    state = state_in;
    phase = phase_in;
}

bool Controller::CheckStateTransition()
{
    if (phase >= kPhaseTransitionThreshold)
        return true;
    else
        return false;
}

bool Controller::CheckCycleTransition()
{
    if (CheckStateTransition() && (state==(num_states-1)))
        return true;
    else
        return false;
}

void Controller::NewState()
{
    state = CleanState(state+1);
    TransitionState(state);
}

void Controller::NewCycle()
{
    TransitionState(0);
}

void Controller::NewCycle(Eigen::VectorXd current_posture)
{
    TransitionState(0);
}
