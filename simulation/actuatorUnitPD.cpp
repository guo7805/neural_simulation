#include "simulation/actuatorUnitPD.h"


ActuatorUnitPD::ActuatorUnitPD()
{

}

ActuatorUnitPD::~ActuatorUnitPD()
{

}

ActuatorUnitPD::ActuatorUnitPD(int inda, int jointID_in, double kp_in, double kd_in, double tl_in)
{
    actuator_id = inda;
    effective_joint_id = jointID_in;
    kp = kp_in;
    kd = kd_in;
    torque_limit = tl_in;
}



double ActuatorUnitPD::CalculateThetaErr()
{
    double current_theta = pJoint->getHingeAngle();
    double theta_err = target_theta - current_theta;
    return theta_err;
}

double ActuatorUnitPD::CalculateVelocityErr(btScalar timeStep)
{
    //double vel_err;
    double current_theta = pJoint->getHingeAngle();
    double current_vel = (current_theta - pre_theta)/timeStep;
    //vel_err = target_vel - current_vel;
    pre_theta = current_theta;
    //return vel_err;
    return current_vel;
}

double ActuatorUnitPD::GetTorque(double timeStep)
{
    double theta_err = CalculateThetaErr();
    double vel_err = CalculateVelocityErr(timeStep);
    double torque = kp*theta_err + kd*vel_err;
    torque = ClampTorque(torque);
    return torque;
}

void ActuatorUnitPD::SetPreTheta(double pre_theta_in)
{
    pre_theta = pre_theta_in;
}

void ActuatorUnitPD::SetTargetTheta(double target_theta_in)
{
    target_theta = target_theta_in;
}

void ActuatorUnitPD::SetControlParams(double param)
{
    SetTargetTheta(param);
}
