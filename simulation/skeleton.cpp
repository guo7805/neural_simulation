#include <iostream>
#include <fstream>
#include "simulation/skeleton.h"
#include "utility/MathUtil.h"


BodyLink*	localCreateBodyLink(float mass, const btTransform& startTransform,btCollisionShape* shape, BodyLink::BodyLinkInfo link_info)
{
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
		shape->calculateLocalInertia(mass,localInertia);

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

#define USE_MOTIONSTATE 1
#ifdef USE_MOTIONSTATE
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	BodyLink* body = new BodyLink(cInfo, link_info);
	body->setContactProcessingThreshold(BT_LARGE_FLOAT);

#else
	btRigidBody* body = new btRigidBody(mass,0,shape,localInertia);
	body->setWorldTransform(startTransform);
#endif//

	//m_dynamicsWorld->addRigidBody(body);

	return body;
}

Skeleton::Skeleton()
{
    global_tr.setIdentity();
//    global_tr.setOrigin(INIT_GLOBAL_POSITION);
}

Skeleton::~Skeleton()
{

}


std::vector<int> Skeleton::GetEndEffectors()
{
    return end_effectors_vector_;
}

Eigen::VectorXd Skeleton::PositionOfCOM(Posture*p)
{
    Eigen::VectorXd pos = link_vector[START_LINK_ID]->PositionOfLink(p);
    return pos;
}
Eigen::VectorXd Skeleton::PositionOfCOM()
{
    btVector3 pos = link_vector[START_LINK_ID]->getCenterOfMassPosition();
    return MathUtil::ConvertBulletToEigen(pos);
}

void Skeleton::SetPositionOfCOM(btVector3 pos)
{
    BodyLink* com = link_vector[0];
    global_tr.setIdentity();
    global_tr.setOrigin(pos);
    com->setCenterOfMassTransform(global_tr);
}

void Skeleton::ResetPositionOfCOM()
{
//    SetPositionOfCOM(INIT_GLOBAL_POSITION);
    BodyLink* com = link_vector[0];
    com->setCenterOfMassTransform(global_tr);
}


Eigen::VectorXd Skeleton::RotationOfCOM()
{
    float yaw=0;
    float pitch=0;
    float roll=0;
    Eigen::VectorXd rot = Eigen::VectorXd::Zero(3);
    btMatrix3x3 rot_mat = link_vector[START_LINK_ID]->getCenterOfMassTransform().getBasis();
    rot_mat.getEulerYPR(yaw, pitch, roll);
    rot << yaw, pitch, roll;
    return rot;
}

Eigen::VectorXd Skeleton::PositionOfLink(Posture*p, int link_id)
{
    Eigen::VectorXd pos = link_vector[link_id]->PositionOfLink(p);
    return pos;
}

void Skeleton::SetPosture(Posture* p)
{
    // Given the posture, compute the position of each body link with forward kinematics
    // Set the position of each body link with motion state
    for(int indl=0; indl<num_links; indl++)
    {
        BodyLink* rb = link_vector[indl];
        btTransform tr = rb->ForwardKinematics(p);
        //if((indl==1)||(indl==4))
        //    std::cout<<tr.getOrigin().x()<< " " <<tr.getOrigin().y() << " " <<tr.getOrigin().z() << std::endl;
        rb -> setCenterOfMassTransform(tr);
        btDefaultMotionState* ms = (btDefaultMotionState*)rb->getMotionState();
        ms->setWorldTransform(tr);
    }
}


void Skeleton::GetPosture(Posture* p)
{
    // Loop over the joints and read the angles
    Eigen::VectorXd posture_data = Eigen::VectorXd::Zero(num_joints);
    for(int indj=0; indj<num_joints; indj++)
    {
        btHingeConstraint* pJoint = joint_vector[indj];
        double angle;
        angle = pJoint->getHingeAngle();
        posture_data[indj] = angle;
    }
    p->SetPostureData(posture_data);
}



std::vector<BodyJoint*> Skeleton::GetJointsOnCurrentChain(BodyLink* link)
{
    std::vector<BodyJoint*> parent_joints_chain = GetParentJointsOnCurrentChain(link);
    std::vector<BodyJoint*> child_joints_chain = GetChildJointsOnCurrentChain(link);
    parent_joints_chain.insert(parent_joints_chain.end(), child_joints_chain.begin(), child_joints_chain.end());
    return parent_joints_chain;
}

std::vector<BodyJoint*> Skeleton::GetParentJointsOnCurrentChain(BodyLink* link)
{
    std::vector<BodyJoint*> joint_chain;
    BodyLink* current_link = link;
    BodyJoint* joint = current_link->GetParentJoint();
    while(joint!=nullptr)
    {
        joint_chain.push_back(joint);
        joint = joint->GetParentJoint();
    }
    return joint_chain;
}

std::vector<BodyJoint*> Skeleton::GetChildJointsOnCurrentChain(BodyLink* link)
{
    std::vector<BodyJoint*> joint_chain;
    BodyLink* current_link = link;
    BodyJoint* joint = current_link->GetChildJoint();
    while(joint!=nullptr)
    {
        joint_chain.push_back(joint);
        joint = joint->GetChildJoint();
    }
    return joint_chain;
}

int Skeleton::GetNumDOFsOnCurrentChain(BodyLink* link)
{
    std::vector<BodyJoint*> joint_chain = GetJointsOnCurrentChain(link);
    return joint_chain.size();
}

int Skeleton::GetNumDOFsOnCurrentChain(BodyJoint* joint)
{
    int num_dofs = 0;
    return num_dofs;
}


Eigen::MatrixXd Skeleton::Jacobian(Posture*p, int link_id)
{
    std::vector<BodyJoint*> joint_chain = GetJointsOnCurrentChain(link_vector[link_id]);
    int NUM_DOFs_PER_LINK = joint_chain.size();
    int NUM_DOFS_ENDEFFECTOR = 3;
    Eigen::MatrixXd jac(NUM_DOFS_ENDEFFECTOR, NUM_DOFs_PER_LINK);

    for(unsigned int indj=0; indj<joint_chain.size(); indj++)
    {
        jac.col(indj) = JacobianOneDOF(p, indj, link_id);
    }
    return jac;
}

Eigen::VectorXd Skeleton::JacobianOneDOF(Posture*p, int joint_id, int link_id)
{
    BodyJoint* joint = joint_vector[joint_id];
    BodyLink* link = link_vector[link_id];
    double angle = (*p)[joint_id];
    btTransform tr_pre = joint->ForwardKinematics(p);
    btTransform tr_rot_diff = joint->GetRotationTransformDiff(angle);
    btTransform tr_rot = joint->GetRotationTransform(angle);
    btTransform tr_link = link->ForwardKinematics(p);
    btTransform tr_pro = tr_rot.inverse()*tr_pre.inverse()*tr_link;
    btTransform tr = tr_pre*tr_rot_diff*tr_pro;
    Eigen::VectorXd jac_row = MathUtil::ConvertBulletToEigen(tr.getOrigin());
    return jac_row;
}

void Skeleton::LoadSkeleton(const std::string& skel_filename)
{
    bool succ;
    Json::Value skel_info;
    succ = JsonUtil::LoadFileIntoJsonValue(skel_filename, skel_info);

    if (succ)
    {
        ParseSkeleton(skel_info);
    }
    else
    {
        std::cout << "Failed to load" << skel_filename << std::endl;
    }

}

void Skeleton::ParseSkeleton(Json::Value skel_info)
{
    Json::Value world_com = skel_info["World_COM"];
    ParseCOM(world_com);
    Json::Value link_info = skel_info["BodyParts"];
    ParseLinks(link_info);
    Json::Value joint_info = skel_info["Joints"];
    ParseJoints(joint_info);
}

void Skeleton::ParseCOM(Json::Value com_info)
{
    int num = com_info.size();
    double* world_tr = new double [num];
    for(int ind=0; ind<num; ind++)
    {
        world_tr[ind] = com_info[ind].asFloat();
    }
    btMatrix3x3 rot;
    rot.setIdentity();
    rot.setEulerZYX(world_tr[3], world_tr[4], world_tr[5]);
    global_tr.setBasis(rot);
    global_tr.setOrigin(btVector3(world_tr[0], world_tr[1], world_tr[2]));
    delete world_tr;
}

void Skeleton::ParseLinks(Json::Value link_info)
{
    num_links = link_info.size();

    for(int indl=0; indl<num_links; indl++)
    {
         BodyLink* link = ParseLinkElement(link_info[indl]);
         link_vector.push_back(link);
         if (IsEndEffector(link_info[indl]))
            end_effectors_vector_.push_back(indl);
    }
}

bool Skeleton::IsEndEffector(Json::Value link_element_info)
{
    std::string name = link_element_info["Name"].asString();
    if(name.find("END")!=std::string::npos)
        return true;
    else
        return false;
}



BodyLink* Skeleton::ParseLinkElement(Json::Value link_element_info)
{
    //std::string name = link_element_info["Name"].asString();
    int Id = link_element_info["Id"].asInt();

    btCollisionShape* link_col_shape = ParseLinkElementShape(link_element_info);
    btTransform tr = ParseLinkElementTransform(link_element_info);
    btScalar mass = ParseLinkElementMass(link_element_info);

    // btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, link_motion_state, link_col_shape);
    BodyLink::BodyLinkInfo link_info;
    link_info.mass = mass;
    link_info.link_id_ = Id;
    link_info.child_joint_ = nullptr;
    link_info.parent_joint_ = nullptr;
    BodyLink* link = localCreateBodyLink(mass, tr, link_col_shape, link_info);
    return link;
}

btCollisionShape* Skeleton::ParseLinkElementShape(Json::Value link_element_info)
{
    std::string shape = link_element_info["Shape"].asString();
    btScalar lenX, lenY, lenZ;
    //if(shape == "box")
    //{
        lenX = link_element_info["LenX"].asFloat();
        lenY = link_element_info["LenY"].asFloat();
        lenZ = link_element_info["LenZ"].asFloat();
        btBoxShape* box_shape = new btBoxShape(btVector3(lenX/2, lenY/2, lenZ/2));  // the input to boxshape is the half extents
        return box_shape;
    //}
}

btTransform Skeleton::ParseLinkElementTransform(Json::Value link_element_info)
{
    btScalar comX, comY, comZ;
    comX = link_element_info["ComX"].asFloat();
    comY = link_element_info["ComY"].asFloat();
    comZ = link_element_info["ComZ"].asFloat();

    btTransform tr;
    tr.setIdentity();
    tr.setOrigin(btVector3(comX, comY, comZ));
    tr = global_tr*tr;
    return tr;
}

btScalar Skeleton::ParseLinkElementMass(Json::Value link_element_info)
{
    //int id = link_element_info["Id"].asInt();
    //if (id == 0)
    //    return 0.0;
    //else
    //    return 1.0;

    btScalar lenX, lenY, lenZ;

    lenX = link_element_info["LenX"].asFloat();
    lenY = link_element_info["LenY"].asFloat();
    lenZ = link_element_info["LenZ"].asFloat();

    btScalar mass = lenX*lenY*lenZ;

    return mass;

}


btDefaultMotionState* Skeleton::ParseLinkElementMotionState(Json::Value link_element_info)
{
    btScalar comX, comY, comZ;
    comX = link_element_info["ComX"].asFloat();
    comY = link_element_info["ComY"].asFloat();
    comZ = link_element_info["ComZ"].asFloat();

    btTransform tr;
    tr.setIdentity();
    tr.setOrigin(btVector3(comX, comY, comZ));
    btDefaultMotionState* ms =new btDefaultMotionState(tr);
    return ms;
}


void Skeleton::ParseJoints(Json::Value joint_info)
{
    num_joints = joint_info.size();


    for(int indj=0; indj<num_joints; indj++)
    {
        BodyJoint* joint = ParseJointElement(joint_info[indj]);
        joint_vector.push_back(joint);
    }

}


BodyJoint* Skeleton::ParseJointElement(Json::Value joint_element_info)
{
    //std::string name = joint_element_info["Name"].asString();

    btVector3 joint_pos;
    int joint_axis;
    ParseJointElementPosition(joint_element_info, joint_pos);
    ParseJointElementAxis(joint_element_info, joint_axis);

    int jointID = joint_element_info["Id"].asInt();
    int objAID = joint_element_info["ObjA"].asInt();
    int objBID = joint_element_info["ObjB"].asInt();

    BodyJoint::BodyJointInfo jInfo;
    jInfo.joint_id_ = jointID;
    jInfo.axis_ = joint_axis;
    jInfo.parent_link_ = link_vector[objAID];
    jInfo.child_link_ = link_vector[objBID];
    jInfo.parent_link_id_ = objAID;
    jInfo.child_link_id_ = objBID;

    BodyJoint* joint = BuildJointElement(jInfo, joint_pos);



    //if (objA==0)
    joint->setDbgDrawSize(btScalar(2.f));
    //joint->setLimit(-SIMD_HALF_PI * 0.5f, SIMD_HALF_PI * 0.5f);
    ParseJointElementLimits(joint_element_info, joint);

    return joint;
}

void Skeleton::ParseJointElementPosition(Json::Value joint_element_info, btVector3& joint_pos)
{

    btScalar posX = joint_element_info["PosX"].asFloat();
    btScalar posY = joint_element_info["PosY"].asFloat();
    btScalar posZ = joint_element_info["PosZ"].asFloat();

    joint_pos.setValue(posX, posY, posZ);
    joint_pos = global_tr*joint_pos;
}

void Skeleton::ParseJointElementAxis(Json::Value joint_element_info, int& joint_axis)
{
    std::string rotate_axis = joint_element_info["RotateAxis"].asString();
    if (rotate_axis == "z")
        joint_axis = BodyJoint::RotationAxis::ROTATION_AXIS_Z;
    else if (rotate_axis == "y")
        joint_axis = BodyJoint::RotationAxis::ROTATION_AXIS_Y;
    else if (rotate_axis == "x")
        joint_axis = BodyJoint::RotationAxis::ROTATION_AXIS_X;
}

void Skeleton::ParseJointElementLimits(Json::Value joint_element_info, btHingeConstraint* joint)
{
    btScalar limHigh = joint_element_info["LimHigh"].asFloat();
    btScalar limLow = joint_element_info["LimLow"].asFloat();

    joint->setLimit(limLow, limHigh);
}

BodyJoint* Skeleton::BuildJointElement(BodyJoint::BodyJointInfo jInfo, const btVector3& joint_pos)
{

    btVector3 joint_axes[3];
    joint_axes[0] = btVector3(1.0, 0.0, 0.0);
    joint_axes[1] = btVector3(0.0, 1.0, 0.0);
    joint_axes[2] = btVector3(0.0, 0.0, 1.0);

    btVector3 joint_axis = joint_axes[jInfo.axis_];
    BodyLink* rbA = jInfo.parent_link_;
    BodyLink* rbB = jInfo.child_link_;

    btVector3 pivotInA = joint_pos - rbA->getCenterOfMassPosition();
    btVector3 pivotInB = joint_pos - rbB->getCenterOfMassPosition();

    BodyJoint* joint = new BodyJoint(*rbA, *rbB, pivotInA, pivotInB, joint_axis, joint_axis, jInfo, true);

    rbA->SetChildJoint(joint);
    rbB->SetParentJoint(joint);
    return joint;
}




void Skeleton::AddAllLinkToWorld(btDynamicsWorld* m_dynamicsWorld)
{
    for(unsigned int indl=0; indl<link_vector.size(); indl++)
    {
        m_dynamicsWorld->addRigidBody(link_vector[indl]);
    }
}


void Skeleton::AddAllJointToWorld(btDynamicsWorld* m_dynamicsWorld)
{
    for(unsigned int indj=0; indj<joint_vector.size(); indj++)
    {
        m_dynamicsWorld->addConstraint(joint_vector[indj], true);
    }
}


void Skeleton::DelAllLinkFromWorld(btDynamicsWorld* m_dynamicsWorld)
{
    for(unsigned int indl=0; indl<link_vector.size(); indl++)
    {
        m_dynamicsWorld->removeRigidBody(link_vector[indl]);
    }
}

void Skeleton::DelAllJointFromWorld(btDynamicsWorld* m_dynamicsWorld)
{
    for(unsigned int indj=0; indj<joint_vector.size(); indj++)
    {
        m_dynamicsWorld->removeConstraint(joint_vector[indj]);
    }
}


void Skeleton::EnableKinematics(btDynamicsWorld* m_dynamicsWorld)
{
    // When user presses the button "["
    // Reference Link: http://www.bulletphysics.org/Bullet/phpBB3/viewtopic.php?f=9&t=4517
    DelAllJointFromWorld(m_dynamicsWorld);
    DelAllLinkFromWorld(m_dynamicsWorld);

    for(unsigned int indl=0; indl<link_vector.size(); indl++)
    {
        link_vector[indl]->setCollisionFlags( link_vector[indl]->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
        link_vector[indl]->setActivationState(DISABLE_DEACTIVATION);
        m_dynamicsWorld->addRigidBody(link_vector[indl]);
    }
    //AddAllLinkToWorld(m_dynamicsWorld);
    AddAllJointToWorld(m_dynamicsWorld);
}


void Skeleton::EnableDynamics(btDynamicsWorld* m_dynamicsWorld)
{
    // When user presses the button "]"
    // Reference Link: http://www.bulletphysics.org/Bullet/phpBB3/viewtopic.php?f=9&t=4517

    DelAllJointFromWorld(m_dynamicsWorld);
    DelAllLinkFromWorld(m_dynamicsWorld);

    for(unsigned int indl=0; indl<link_vector.size(); indl++)
    {
        BodyLink* old_p = link_vector[indl];
        link_vector[indl] = old_p->RebuildDynamics();
        delete old_p;
    }
    AddAllLinkToWorld(m_dynamicsWorld);


    for(unsigned int indj=0; indj<joint_vector.size(); indj++)
    {
        BodyJoint* old_p = joint_vector[indj];
        joint_vector[indj] = old_p->RebuildDynamics(link_vector);
        delete old_p;
    }
    AddAllJointToWorld(m_dynamicsWorld);
}

