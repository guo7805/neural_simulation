#include "simulation/actuator.h"
#include "simulation/actuatorUnitPD.h"

Actuator::Actuator()
{
    num_actuator = 0;
}

Actuator::~Actuator()
{

}


void  Actuator::LoadActuator(std::string actuator_file_name)
{
    bool succ;
    Json::Value actuator_info;
    succ = JsonUtil::LoadFileIntoJsonValue(actuator_file_name, actuator_info);

    if (succ)
    {
        LoadActuatorVector(actuator_info);
    }
    else
    {
        std::cout << "Failed to load " << actuator_file_name << std::endl;
    }

}

void Actuator::LoadActuatorVector(Json::Value actuator_info)
{
    num_actuator = actuator_info.size();

    for(int inda=0; inda<num_actuator; inda++)
    {
        ActuatorUnit* act = LoadActuatorPDUnit(actuator_info[inda]);
        actuator_vector.push_back(act);
    }

}

ActuatorUnit* Actuator::LoadActuatorPDUnit(Json::Value actuator_unit_info)
{
    int inda = actuator_unit_info["actuatorID"].asInt();
    int jointID = actuator_unit_info["jointID"].asInt();
    double kp = actuator_unit_info["kp"].asFloat();
    double kd = actuator_unit_info["kd"].asFloat();
    double tl = actuator_unit_info["limit"].asFloat();
    ActuatorUnit* act = new ActuatorUnitPD(inda, jointID, kp, kd, tl);
    return act;
}
