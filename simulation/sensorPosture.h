#ifndef SENSORPOSTURE_H
#define SENSORPOSTURE_H

#include "simulation/skeleton.h"



class SensorPosture
{
public:
    SensorPosture();
    ~SensorPosture();
    SensorPosture(Skeleton* skel);

    void SetSkeleton(Skeleton* skel);

    Eigen::VectorXd GetPostureVector();
    Eigen::VectorXd GetCOMPosition();
    Eigen::VectorXd GetCOMRotation();
    Eigen::VectorXd GetCOMTransformVector();
    Eigen::VectorXd PositionOfEndEffectors(bool local=false);
private:
    Skeleton* skel_;
};



#endif // SENSORPOSTURE_H
