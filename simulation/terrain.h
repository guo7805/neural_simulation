#ifndef TERRAIN_H
#define TERRAIN_H

#include "btBulletDynamicsCommon.h"

class Terrain
{
public:
    enum TERRAIN_TYPE
	{
		TERRAIN_FLAT,
		TERRAIN_STAIRS,
	};
    Terrain();
    virtual ~Terrain();
    virtual void init_terrain();
    virtual btCollisionShape* get_terrain_shape()=0;
    virtual btRigidBody* get_terrain_body()=0;
    virtual void set_maxWidth(btScalar mw) { maxWidth=mw; };
    virtual void set_maxLength(btScalar ml){ maxLength=ml;};
    virtual void set_maxHeight(btScalar mh){ maxHeight=mh;};
protected:
    int type;
    int maxWidth;
    int maxLength;
    int maxHeight;
    btTransform terrainTransform;
};


#endif // TERRAIN_H
