#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include "Eigen/Dense"
#include "Eigen/StdVector"

#include "simulation/skeleton.h"
#include "simulation/actuatorSys.h"
#include "simulation/controller.h"
#include "simulation/controllerKinematic.h"
#include "simulation/sensorSys.h"
#include "learning/neuralNet.h"
#include "utility/JsonUtil.h"
#include "utility/LogUtil.h"

class Character
{

public:
    Character();
    Character(std::string skel_file_name);
    Character(std::string skel_file_name, std::string actuator_file_name, std::string controller_file_name);
    virtual ~Character();
    void    LoadActuatorSys(std::string actuatorSys_file_name);
    void    LoadSensorSys(std::string sensorSys_file_name);
    void    LoadController(std::string controller_file_name);
    void    LoadNetwork(std::string nn_file_name);
    void    LoadNetwork(NeuralNet* net);
    void    LoadNetworkModel(std::string model_file_name);
    void    LoadKinematic(std::string motion_file_name);
    void    LinkSkeletonWithActuator();


    Skeleton*   GetSkeleton()   {return skeleton_;};
    Controller* GetController() {return controller_;};
    ActuatorSys*   GetActuatorSys()   {return actuator_sys_;};
    SensorSys*  GetSensorSys()  {return sensor_sys_;};
    bool        GetKinematicFlag() {return is_kinematic_;};
    bool    FallOff();
    Eigen::VectorXd GetCOMPosition();

    void    SetKinematicFlag(bool kFlag);
    virtual double  GetCycleDuration();

    // Kinemaitc Simulation
    void    UpdateKinematicState(double time_step);
    void    EnableKinematics(btDynamicsWorld* m_dynamicsWorld);
    bool    IsMotionFinished();
    void    ResetInitialPose();

    // Dynamic Simulation
    void    EnableDynamics(btDynamicsWorld* m_dynamicsWorld);
    void    ApplyTorque(double timeStep);
    void    UpdateDynamicState(double timeStep);
    void    UpdateActuatorControlParams(Eigen::VectorXd control_params);
    void    InitLocomotion();
    void    InitPosture(Posture* p);
    Logger*     cLogger;

    Eigen::VectorXd GetSensoryState();
    Eigen::VectorXd GetControlState();
    Eigen::VectorXd IntegrateState();
    Eigen::VectorXd GetPostureVector();

private:
    bool        is_kinematic_;
    Skeleton*   skeleton_;
    Controller* controller_;                    // in charge of dynamic simulation
    ControllerKinematic* controller_kinematic_;  // in charge of kinematic simulation
    ActuatorSys*    actuator_sys_;
    SensorSys*      sensor_sys_;


};


#endif // CHARACTER_H
