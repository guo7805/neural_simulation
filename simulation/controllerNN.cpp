#include "simulation/controllerNN.h"


ControllerNN::ControllerNN() : Controller()
{
    mNet_ = new NeuralNet();
}

ControllerNN::~ControllerNN()
{

}

void ControllerNN::Reset()
{

}

void ControllerNN::Clear()
{

}

void ControllerNN::Update(double time_step)
{
    Controller::Update(time_step);
}


void ControllerNN::LoadNet(const std::string& net_file)
{
    mNet_->Clear();
    mNet_->LoadNet(net_file);
}

void ControllerNN::LoadNet(NeuralNet* net)
{
    mNet_ = net;
}

void ControllerNN::LoadModel(const std::string& model_file)
{
    mNet_->LoadModel(model_file);
}

void ControllerNN::LoadScale(const std::string& scale_file)
{
    mNet_->LoadScale(scale_file);
}

int ControllerNN::GetNetInputSize() const
{
    return 0;
}

int ControllerNN::GetNetOutputSize() const
{
    return 0;
}

//void ControllerNN::SetTarget(Eigen::VectorXd target)
//{
//    target_ = target;
//}
//
//void ControllerNN::SetInitial(Eigen::VectorXd initial)
//{
//    initial_ = initial;
//}


//Eigen::VectorXd ControllerNN::DecideTargetPosition()
//{
//    Eigen::VectorXd target = initial_;
//    target[2] = -target[2];
//    return target;
//}

//Eigen::VectorXd ControllerNN::GetCtrlState()
//{
//    return target_;
//}

//Eigen::VectorXd ControllerNN::IntegrateInput(Eigen::VectorXd external_input)
//{
//    int input_size = mNet_->GetInputSize();
//    Eigen::VectorXd input_state  = Eigen::VectorXd::Zero(input_size);
//    //input_state << external_input, target_;
//    return input_state;
//}

Eigen::VectorXd ControllerNN::DecideAction(Eigen::VectorXd input_state)
{
    Eigen::VectorXd action;

    //input_state = IntegrateInput(external_input);
    action = mNet_->Evaluate(input_state);
    //action = Eigen::VectorXd::Zero(12);
    return action;
}

void ControllerNN::NewState()
{
    Controller::NewState();
}

