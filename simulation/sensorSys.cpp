#include "sensorSys.h"


SensorSys::SensorSys()
{

}

SensorSys::~SensorSys()
{
    delete sensor_posture_;
}

SensorSys::SensorSys(Skeleton* skel_)
{
    sensor_posture_ = new SensorPosture(skel_);
}


Eigen::VectorXd SensorSys::GetCurrentState()
{
    Eigen::VectorXd current_state = sensor_posture_->PositionOfEndEffectors(true);
    return current_state;
}

Eigen::VectorXd SensorSys::GetCOMRotation()
{
    Eigen::VectorXd rot = sensor_posture_->GetCOMRotation();
    return rot;
}

Eigen::VectorXd SensorSys::GetPostureVector()
{
    Eigen::VectorXd pose = sensor_posture_->GetPostureVector();
    return pose;
}

Eigen::VectorXd SensorSys::GetCOMPosition()
{
    Eigen::VectorXd pos = sensor_posture_->GetCOMPosition();
    return pos;
}

Eigen::VectorXd SensorSys::IntegrateSensor()
{
    Eigen::VectorXd pose = sensor_posture_->GetPostureVector();
    Eigen::VectorXd com_rot = sensor_posture_->GetCOMRotation();
    Eigen::VectorXd com_pos = sensor_posture_->GetCOMPosition();
    Eigen::VectorXd sensor_state(pose.size()+com_rot.size()+com_pos.size());
    sensor_state << pose, com_rot, com_pos;
    return sensor_state;
}
