#ifndef CONTROLLERKINEMATIC_H
#define CONTROLLERKINEMATIC_H

#include "simulation/posture.h"
#include "simulation/motion.h"


class ControllerKinematic
{
public:
    ControllerKinematic();
    ~ControllerKinematic();
    ControllerKinematic(std::string motion_file_name);


    Posture* Update(double time_step);
    bool IsMotionFinished();
    void ComputeTorque();
    void Reset();
private:
    Motion*     motion_;
};


#endif // CONTROLLERKINEMATIC_H
