#include "simulation/motion.h"


Motion::Motion()
{
    posture_id_ = 0;
    num_postures_ = 0;
}

Motion::Motion(std::string motion_file_name)
{
    Json::Value motion_info;
    JsonUtil::LoadFileIntoJsonValue(motion_file_name, motion_info);
    num_postures_ = motion_info.size();

    for(int indp=0; indp<num_postures_; indp++)
    {
        Posture* p = new Posture(motion_info[indp]);
        motion_data_.push_back(p);
    }
    posture_id_ = 0;

}

Motion::~Motion()
{

}

void Motion::SetMotionData(std::vector<Posture*> mData)
{
    motion_data_ = mData;
}

Posture* Motion::GetPosture()
{
    if(posture_id_<num_postures_)
        return motion_data_[posture_id_];
    else
        std::cout << "Posture ID out of range" << std::endl;
}

Posture* Motion::PerformNextPosture()
{
    posture_id_++;
    if(posture_id_>(num_postures_-1))
        posture_id_ = num_postures_-1;
    Posture* p = GetPosture();
    return p;
}

bool Motion::IsFinished()
{
    if (posture_id_<(num_postures_-1))
        return false;
    else
        return true;
}

void Motion::Reset()
{
    posture_id_ = 0;
}
