#include "simulation/controllerNNPDHexapod.h"


ControllerNNPDHexapod::ControllerNNPDHexapod()
{

}

ControllerNNPDHexapod::~ControllerNNPDHexapod()
{

}

void ControllerNNPDHexapod::SetControlState(Eigen::VectorXd inputs)
{
    control_state_ = inputs;
}


Eigen::VectorXd ControllerNNPDHexapod::GetControlState()
{
    return control_state_;
}


Eigen::VectorXd ControllerNNPDHexapod::InitControlState()
{
    control_state_ = Eigen::VectorXd::Zero(3);
    return control_state_;
}

//void ControllerNN::NewCycle(Eigen::VectorXd control_inputs)
//{
//    SetControlState(control_inputs);
//    Eigen::VectorXD input_state = ;
//    ControllerNNPD::NewCycle(input_state);
//}

