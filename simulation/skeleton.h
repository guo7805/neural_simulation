#ifndef SKELETON_H
#define SKELETON_H

#include <string>
#include <vector>
#include "utility/JsonUtil.h"
#include "simulation/posture.h"
#include "simulation/bodyJoint.h"
#include "simulation/bodyLink.h"

const int START_LINK_ID = 0;


class Skeleton
{
public:

    Skeleton();
    virtual ~Skeleton();
    std::vector<BodyLink*> GetLinkVector(){return link_vector;};
    std::vector<BodyJoint*> GetJointVector(){return joint_vector;};
    int GetNumJoints(){return num_joints;};
    int GetNumLinks() {return num_links;};

    void LoadSkeleton(const std::string& skel_name);
    void ParseSkeleton(Json::Value skel_info);
    void ParseCOM(Json::Value com_info);

    void        ParseLinks(Json::Value link_info);
    BodyLink*   ParseLinkElement(Json::Value link_element_info);
    btCollisionShape*   ParseLinkElementShape(Json::Value link_element_info);
    btTransform         ParseLinkElementTransform(Json::Value link_element_info);
    btScalar            ParseLinkElementMass(Json::Value link_element_info);
    btDefaultMotionState* ParseLinkElementMotionState(Json::Value link_element_info);


    void        ParseJoints(Json::Value joint_info);
    BodyJoint*  ParseJointElement(Json::Value joint_element_info);
    BodyJoint*  BuildJointElement(BodyJoint::BodyJointInfo jInfo, const btVector3& joint_pos);
    void        ParseJointElementPosition(Json::Value joint_element_info, btVector3& joint_pos);
    void        ParseJointElementAxis(Json::Value joint_element_info, int& joint_axis);
    void        ParseJointElementLimits(Json::Value joint_element_info, btHingeConstraint* joint);

    void SetPosture(Posture* p);
    void GetPosture(Posture* p);

    int GetNumDOFsOnCurrentChain(BodyLink* link);
    int GetNumDOFsOnCurrentChain(BodyJoint* joint);
    std::vector<BodyJoint*> GetJointsOnCurrentChain(BodyLink* link);
    std::vector<BodyJoint*> GetParentJointsOnCurrentChain(BodyLink* link);
    std::vector<BodyJoint*> GetChildJointsOnCurrentChain(BodyLink* link);

    // Jacobian matrix of a particular link
    Eigen::MatrixXd Jacobian(Posture*p, int link_id);
    // one row in Jacobian matrix. The position of a particular link with respect to a particular joint
    Eigen::VectorXd JacobianOneDOF(Posture*p, int joint_id, int link_id);

    std::vector<int> GetEndEffectors();
    bool IsEndEffector(Json::Value link_info);
    Eigen::VectorXd PositionOfCOM();
    Eigen::VectorXd RotationOfCOM();
    Eigen::VectorXd PositionOfCOM(Posture*p);
    Eigen::VectorXd PositionOfLink(Posture*p, int link_id);
    void SetPositionOfCOM(btVector3 pos);
    void ResetPositionOfCOM();

    void EnableKinematics(btDynamicsWorld* m_dynamicsWorld);
    void EnableDynamics(btDynamicsWorld* m_dynamicsWorld);
    void AddAllLinkToWorld(btDynamicsWorld* m_dynamicsWorld);
    void AddAllJointToWorld(btDynamicsWorld* m_dynamicsWorld);
    void DelAllLinkFromWorld(btDynamicsWorld* m_dynamicsWorld);
    void DelAllJointFromWorld(btDynamicsWorld* m_dynamicsWorld);
private:
    int num_joints;
    int num_links;
    btTransform global_tr;
    std::vector<BodyLink*> link_vector;
    std::vector<BodyJoint*> joint_vector;
    // Since the pointers to the body links are updated when the character switches between kinematic and dynamic
    // We use indices to keep record of the end effectors, not using pointers
    std::vector<int> end_effectors_vector_;
};

#endif // SKELETON_H
