#ifndef ACTUATOR_H
#define ACTUATOR_H

#include <vector>
#include "simulation/actuatorUnit.h"
#include "utility/JsonUtil.h"

class ActuatorSys
{
public:
    Actuator();
    ~Actuator();
    int     GetNumActuator(){return num_actuator;};
    std::vector<ActuatorUnit*>   GetActuatorVector(){return actuator_vector;};


    void    LoadActuator(std::string actuator_file_name);
    void    LoadActuatorVector(Json::Value actuator_info);
    ActuatorUnit*   LoadActuatorPDUnit(Json::Value actuator_unit_info);

private:
    int num_actuator;
    std::vector<ActuatorUnit*> actuator_vector;
};


#endif // ACTUATOR_H
