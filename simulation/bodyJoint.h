#ifndef BODYJOINT_H
#define BODYJOINT_H

#include "btBulletDynamicsCommon.h"
#include "simulation/bodyLink.h"
#include "simulation/posture.h"
#include "utility/JsonUtil.h"

class BodyLink;



class BodyJoint : public btHingeConstraint
{
public:

    enum RotationAxis
    {
        ROTATION_AXIS_X,
        ROTATION_AXIS_Y,
        ROTATION_AXIS_Z,
    };

    struct BodyJointInfo
    {
        int joint_id_;
        int axis_;
        int parent_link_id_;
        int child_link_id_;
        BodyLink* parent_link_;
        BodyLink* child_link_;
    };

    BodyJoint(BodyLink &rbA, BodyLink &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB,
              const btVector3 &axisInA, const btVector3 &axisInB, bool useReferenceFrameA=false);
    BodyJoint(BodyLink &rbA, BodyLink &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB,
              const btVector3 &axisInA, const btVector3 &axisInB, BodyJointInfo bjInfo, bool useReferenceFrameA=false);
    BodyJoint(BodyLink &rbA, BodyLink &rbB, const btTransform &frameA, const btTransform &frameB,
              BodyJointInfo bjInfo, bool useReferenceFrameA=false);

    BodyJoint* RebuildDynamics(std::vector<BodyLink*> link_vector);

    int GetJointID(){ return joint_info_.joint_id_;};
    int GetJointAxis(){ return joint_info_.axis_;};
    BodyLink* GetParentLink(){ return joint_info_.parent_link_;};
    BodyLink* GetChildLink(){  return joint_info_.child_link_;};
    BodyJoint* GetParentJoint();
    BodyJoint* GetChildJoint();

    int CheckRotationVector(btVector3 vec);
    btVector3 GetOriginalRotationVector();
    // calculate the transform of joint frame, given the joint rotation angle
    btTransform GetRotationTransform(double angle);
    // calculate the differentiation of the joint transform, with respect to the joint rotation angle
    btTransform GetRotationTransformDiff(double angle);
    // calculate the preceeding transform of the joint (without considering the transform the current jont frame
    btTransform ForwardKinematics(Posture* p, bool include_joint_rotation=true);
    // calculate the transformation of child link in the current joint frame
    btTransform ChildLinkInJointFrame(Posture* p, int link_id);


    void SetParentLinkID(int pID){ joint_info_.parent_link_id_ = pID;};
    void SetChildLinkID(int cID) { joint_info_.child_link_id_ = cID; };
    void SetParentLink(BodyLink* pL) {joint_info_.parent_link_ = pL;};
    void SetChildLink(BodyLink* cL)  {joint_info_.child_link_ = cL;};

    ~BodyJoint();

private:
    BodyJointInfo joint_info_;
};

#endif // BODYLINK_H
