#ifndef ACTUATORPD_H
#define ACTUATORPD_H



#include "actuatorUnit.h"




class ActuatorUnitPD : public ActuatorUnit
{
public:

	ActuatorUnitPD();
	ActuatorUnitPD(int inda, int jointID_in, double kp_in, double kd_in, double tl_in);
	virtual ~ActuatorUnitPD();

	virtual double  GetTorque(double timeStep);
    virtual void    SetControlParams(double param);



    void    SetPreTheta(double pre_theta_in);
    void    SetTargetTheta(double target_theta_in);

    double  CalculateThetaErr();
    double  CalculateVelocityErr(btScalar timeStep);


private:
    double kp;
    double kd;
    double pre_theta;
    double target_theta;
    double target_vel;
};

#endif // ACTUATORPD_H
