#include "simulation/controllerDynamic.h"

ControllerDynamic::ControllerDynamic()
{

}


ControllerDynamic::ControllerDynamic(Json::Value args, btDynamicsWorld* m_dynamicsWorld)
{

}


ControllerDynamic::~ControllerDynamic()
{
    delete actuator_sys_;
    delete sensor_sys_;

}

void ControllerDynamic::InitPosture(std::string posture_file_name)
{
//    posture_ = new Posture(posture_file_name);
}




void ControllerDynamic::LinkSkeletonWithActuator()
{
    std::vector<BodyJoint*> joint_vector = skeleton_->GetJointVector();
    std::vector<ActuatorUnit*> actuator_vector = actuator_->GetActuatorVector();

    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        int indj = actuator_vector[inda]->GetEffectiveJointID();
        actuator_vector[inda]->SetEffectiveJoint(joint_vector[indj]);
    }
}


void ControllerDynamic::Update(double time_step)
{
    Controller::Update(time_step);
    // check whether the current locomotion cycle finishes
    if (CheckCycleTransition())
    {
        // if the current cycle finishes, call the NN controller to update the control parameter
        //Eigen::VectorXd current_posture = sensor_posture_->PositionOfEndEffector(true);
        //NewCycle(current_posture);
    }
    else
    {
        if (CheckStateTransition())
        {
            // update the state
            // GetBlendControlParams() will fetch the correct control parameter in the controller class
            NewState();
        }
    }


    // update the actuator target angle with the blend control parameter
    Eigen::VectorXd control_params = GetBlendControlParams();
    UpdateActuatorControlParams(control_params);
}




void ControllerDynamic::ApplyTorque(double timeStep)
{
    std::vector<ActuatorUnit*> actuator_vector = actuator_sys_->GetActuatorVector();
    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        double torque = actuator_vector[inda]->GetTorque(timeStep);
        actuator_vector[inda]->ApplyTorque(torque);
    }

}

void ControllerDynamic::UpdateActuatorControlParams(Eigen::VectorXd control_params)
{
    // Maybe should check whether the dimensions of the control_params match the number of actuators
    // Update the actuator parameter at the level of per simulation step. Other updates are operated on the controller level
    std::vector<ActuatorUnit*> actuator_vector = actuator_->GetActuatorVector();
    for(unsigned int inda=0; inda<actuator_vector.size(); inda++)
    {
        actuator_vector[inda]->SetControlParams(control_params[inda]);
    }

}
