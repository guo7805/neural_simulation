#ifndef ACTUATORMUSCLEHILL_H
#define ACTUATORMUSCLEHILL_H

#include "actuatorUnit.h"


class ActuatorMuscleHill::public ActuatorUnit
{
public:
	ActuatorMuscleHill();
	~ActuatorMuscleHill();
	btScalar force_length();
	btScalar force_velocity();
private:

};


#endif // ACTUATORMUSCLEHILL_H
