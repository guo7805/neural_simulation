#include "simulation/posture.h"


Posture::Posture()
{

}

Posture::~Posture()
{

}

Posture::Posture(std::string posture_file_name)
{

    bool succ;
    Json::Value posture_info;
    succ = JsonUtil::LoadFileIntoJsonValue(posture_file_name, posture_info);

    if (succ)
    {
        ParsePosture(posture_info);
    }
    else
    {
        std::cout << "Failed to load " << posture_file_name << std::endl;
    }

}

Posture::Posture(Json::Value posture_info)
{
    ParsePosture(posture_info);
}

void Posture::ParsePosture(Json::Value posture_info)
{
    int num_dofs = posture_info.size();
    posture_data_ = Eigen::VectorXd::Zero(num_dofs);

    for(int indd=0; indd<num_dofs; indd++)
    {
        double val = posture_info[indd].asFloat();
        posture_data_[indd] = val;
    }
}

void Posture::SetPostureData(Eigen::VectorXd pData)
{
    posture_data_ = pData;
}


double Posture::GetJointData(int jointID)
{
    return posture_data_[jointID];
}

double Posture::operator[](int joint_id)
{
    return GetJointData(joint_id);
}
