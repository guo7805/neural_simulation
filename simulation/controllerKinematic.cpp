#include "simulation/controllerKinematic.h"


ControllerKinematic::ControllerKinematic()
{

}
ControllerKinematic::~ControllerKinematic()
{
    delete motion_;
}

ControllerKinematic::ControllerKinematic(std::string motion_file_name)
{
    assert(motion_file_name!="");
    motion_ = new Motion(motion_file_name);

}


Posture* ControllerKinematic::Update(double time_step)
{
    Posture* posture_ = motion_->GetPosture();
    motion_->PerformNextPosture();
    return posture_;
}

bool ControllerKinematic::IsMotionFinished()
{
    bool is_finished = motion_->IsFinished();
    return is_finished;
}

void ControllerKinematic::Reset()
{
    motion_->Reset();
}

void ControllerKinematic::ComputeTorque()
{
    //        Eigen::VectorXd force = sensor_force_->GetForce(motion_->GetPostureID());
//        Eigen::MatrixXd jac = skeleton_->Jacobian(posture_, 4);
//        Eigen::VectorXd torque = jac.transpose()*force;
//        if(motion_->IsFinished())
//        {
//            if(!cLogger->IsEmpty("torque"))
//            {
//                cLogger->WriteLog("torque.txt", "torque");
//                cLogger->ClearLog("torque");
//            }
//        }
//        else
//        {
//            cLogger->AppendLog(torque, "torque");
//        }
}

