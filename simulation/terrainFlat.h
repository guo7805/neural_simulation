#ifndef TERRAIN_FLAT_H
#define TERRAIN_FLAT_H

#include "simulation/terrain.h"

class TerrainFlat : public Terrain
{
private:
    btBoxShape* terrainShape;
    btRigidBody* terrainBody;
public:
    TerrainFlat();
    TerrainFlat(btVector3 terrain_size, btVector3 terrain_pos);
    virtual ~TerrainFlat();
    virtual btBoxShape* get_terrain_shape();
    virtual btRigidBody* get_terrain_body();
};


#endif // TERRAIN_FLAT_H
