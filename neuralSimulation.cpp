/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/





#include "neuralSimulation.h"
#include "GlutStuff.h"
///btBulletDynamicsCommon.h is the main Bullet include file, contains most common include files.
#include "btBulletDynamicsCommon.h"

#include <stdio.h> //printf debugging
#include "GLDebugDrawer.h"
#include "LinearMath/btAabbUtil2.h"



static GLDebugDrawer gDebugDraw;

///The MyOverlapCallback is used to show how to collect object that overlap with a given bounding box defined by aabbMin and aabbMax.
///See m_dynamicsWorld->getBroadphase()->aabbTest.
struct	MyOverlapCallback : public btBroadphaseAabbCallback
{
    btVector3 m_queryAabbMin;
    btVector3 m_queryAabbMax;

    int m_numOverlap;
    MyOverlapCallback(const btVector3& aabbMin, const btVector3& aabbMax ) : m_queryAabbMin(aabbMin),m_queryAabbMax(aabbMax),m_numOverlap(0)	{}
    virtual bool	process(const btBroadphaseProxy* proxy)
    {
        btVector3 proxyAabbMin,proxyAabbMax;
        btCollisionObject* colObj0 = (btCollisionObject*)proxy->m_clientObject;
        colObj0->getCollisionShape()->getAabb(colObj0->getWorldTransform(),proxyAabbMin,proxyAabbMax);
        if (TestAabbAgainstAabb2(proxyAabbMin,proxyAabbMax,m_queryAabbMin,m_queryAabbMax))
        {
            m_numOverlap++;
        }
        return true;
    }
};


InsectDemo::InsectDemo()
{
    m_cameraDistance = 20.0;
    m_ele = 30.f;
    m_azi = -150.f;
    m_cameraTargetPosition = btVector3(0.f, 5.f, 0.0);
}

InsectDemo::~InsectDemo()
{
    exitPhysics();
}

void InsectDemo::clientMoveAndDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //simple dynamics world doesn't handle fixed-time-stepping
    float ms = getDeltaTimeMicroseconds();

    ///step the simulation
    if (m_dynamicsWorld)
    {
        //The first and third parameters to stepSimulation are measured in seconds, and not milliseconds.
        //http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World
        m_dynamicsWorld->stepSimulation(ms / 1000000.f, 10, 1.0/120.0);
        //optional but useful: debug drawing
        m_dynamicsWorld->debugDrawWorld();

        btVector3 aabbMin(1,1,1);
        btVector3 aabbMax(2,2,2);

        MyOverlapCallback aabbOverlap(aabbMin,aabbMax);
        m_dynamicsWorld->getBroadphase()->aabbTest(aabbMin,aabbMax,aabbOverlap);

        if (aabbOverlap.m_numOverlap)
            printf("#aabb overlap = %d\n", aabbOverlap.m_numOverlap);
    }

    renderme();

    glFlush();

    swapBuffers();

}



void InsectDemo::displayCallback(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderme();

    //optional but useful: debug drawing to detect problems
    if (m_dynamicsWorld)
        m_dynamicsWorld->debugDrawWorld();

    glFlush();
    swapBuffers();
}

void simulationPreTickCallback (btDynamicsWorld *world, btScalar timeStep)
{
    //std::cout << "The world steps forward with " << timeStep << " seconds" << std::endl;
    InsectDemo* app = (InsectDemo*)world->getWorldUserInfo();
    app->UpdateSimulationStep(timeStep);
}

void InsectDemo::UpdateSimulationStep(double timeStep)
{

    if(charac_->GetKinematicFlag())
    {
        charac_->UpdateKinematicState(timeStep);
            // If the character mode is dynamic and the motion is finished, switch to dynamic automatically
        if((charac_mode_=="dynamic")&&(charac_->IsMotionFinished()))
            SwitchKinematicToDynamic();
    }
    else
    {
        charac_->ApplyTorque(timeStep);
        charac_->UpdateDynamicState(timeStep);
    }
}




void	InsectDemo::initPhysics()
{
    setTexturing(true);
    setShadows(true);

    //setCameraDistance(btScalar(50.));

    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();
    //m_collisionConfiguration->setConvexConvexMultipointIterations();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);

    m_broadphase = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);
    m_dynamicsWorld->setDebugDrawer(&gDebugDraw);

    m_dynamicsWorld->setGravity(btVector3(0,-10,0));

    m_dynamicsWorld->setInternalTickCallback(simulationPreTickCallback,this,true);
}

void InsectDemo::initTerrain()
{
    btVector3 terrain_size(50.0, 50.0, 50.0);
    btVector3 terrain_pos(0,-50,0);
    terrain_ = new TerrainFlat(terrain_size, terrain_pos);
    btCollisionShape* terrainShape = terrain_->get_terrain_shape();
    m_collisionShapes.push_back(terrainShape);

    btRigidBody* terrainBody = terrain_->get_terrain_body();
    m_dynamicsWorld->addRigidBody(terrainBody);
}


void InsectDemo::initCharacter(const Json::Value& args)
{

    charac_mode_ = args["mode"].asString();

    std::string skel_file_name = args["skeleton"].asString();
    charac_ = new Character(skel_file_name);

    //A kinematic controller is needed, no matter the character is kinematic or dynamic
    //If the character is dynamic, the motion is to initialize the posture from which the dynamic simulation starts
    charac_->SetKinematicFlag(true);
    Skeleton* skel = charac_->GetSkeleton();
    skel->EnableKinematics(m_dynamicsWorld);
    std::string motion_file_name = args["motion"].asString();
    charac_->LoadKinematic(motion_file_name);


    std::string actuator_file_name = args["actuator"].asString();
    std::string controller_file_name = args["control"].asString();
    std::string neural_file_name = args["neural"].asString();
    std::string model_file_name = args["model"].asString();

    if((actuator_file_name=="")||(controller_file_name=="")||(neural_file_name==""))
    {
        //If there are empty fields in actuator/controller/neural, charac_mode must be kinematic
        assert(charac_mode_=="kinematic");
    }
    else
    {
        charac_->LoadActuatorSys(actuator_file_name);
        charac_->LoadController(controller_file_name);
        charac_->LoadNetwork(neural_file_name);
        if(model_file_name!="")
            charac_->LoadNetworkModel(model_file_name);
        charac_->LoadSensorSys("");
    }
}

bool InsectDemo::LoadArgs(char* argv, Json::Value& args)
{
    std::string arg_file(argv);
    bool succ = JsonUtil::LoadFileIntoJsonValue(arg_file, args);
    if (!succ)
        std::cout << "Fail to load the argument file" << std::endl;
    return succ;
}

void	InsectDemo::clientResetScene()
{
    exitPhysics();
    initPhysics();
}

void InsectDemo::resetCameraPosition(btVector3 cameraPos)
{
    m_ele = 50;
    //m_cameraPosition = cameraPos;
}


void InsectDemo::draw_unit_coordinate()
{
    std::cout << "draw unit coordinate" << std::endl;
}

void	InsectDemo::exitPhysics()
{

    //cleanup in the reverse order of creation/initialization

    //remove the rigidbodies from the dynamics world and delete them
    int i;
    for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ; i--)
    {
        btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }
        m_dynamicsWorld->removeCollisionObject( obj );
        delete obj;
    }

    //delete collision shapes
    for (int j=0; j<m_collisionShapes.size(); j++)
    {
        btCollisionShape* shape = m_collisionShapes[j];
        delete shape;
    }
    m_collisionShapes.clear();

    delete terrain_;
    delete charac_;

    delete m_dynamicsWorld;

    delete m_solver;

    delete m_broadphase;

    delete m_dispatcher;

    delete m_collisionConfiguration;


}


void InsectDemo::keyboardCallback(unsigned char key, int x, int y)
{
    //Skeleton* skel = charac_->GetSkeleton();
    switch (key)
    {
    case '[':
        SwitchDynamicToKinematic();
        break;
    case ']':
        SwitchKinematicToDynamic();
        //m_fMuscleStrength *= 1.1f;
        break;
    default:
        DemoApplication::keyboardCallback(key, x, y);
    }
}

void InsectDemo::SwitchKinematicToDynamic()
{
    charac_->SetKinematicFlag(false);
    charac_->EnableDynamics(m_dynamicsWorld);
    charac_->LinkSkeletonWithActuator();
    charac_->InitLocomotion();
}

void InsectDemo::SwitchDynamicToKinematic()
{
    charac_->SetKinematicFlag(true);
    charac_->EnableKinematics(m_dynamicsWorld);
}



