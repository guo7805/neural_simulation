#ifndef FILEUTIL_H
#define FILEUTIL_H

#include<string>
#include<fstream>

namespace FileUtil{

    std::string RemoveExtension(const std::string& filename);

}


#endif // FILEUTIL_H
