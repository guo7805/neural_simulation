#include "utility/FileUtil.h"


namespace FileUtil
{

std::string RemoveExtension(const std::string& filename)
{
	size_t first_not_dot = filename.find_first_not_of('.');
	size_t last_dot = filename.find_last_of(".");
	if (last_dot == std::string::npos
		|| last_dot <= first_not_dot)
	{
		return filename;
	}
	return filename.substr(0, last_dot);
}

}
