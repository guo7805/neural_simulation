#include "utility/MathUtil.h"



namespace MathUtil
{
int Clamp(int val, int min_val, int max_val)
{
    return std::max(min_val, std::min(val, max_val));
}


double Clamp(double val, double min_val, double max_val)
{
    return std::max(min_val, std::min(val, max_val));
}

Eigen::VectorXd ConcatVectors(Eigen::VectorXd v1, Eigen::VectorXd v2)
{
    Eigen::VectorXd vec_joined( v1.rows() + v2.rows() );
    vec_joined << v1, v2;
    return vec_joined;
}


Eigen::VectorXd ConvertBulletToEigen(btVector3 bullet_vec)
{
    Eigen::VectorXd eigen_vec = Eigen::VectorXd::Zero(3);
    eigen_vec << bullet_vec.x(), bullet_vec.y(), bullet_vec.z();
    return eigen_vec;
}

btMatrix3x3 RotationMatrixDiff(btMatrix3x3 in_mat, int axis)
{
    btMatrix3x3 diff_mat;
    if (axis == 0)  // rotate around x axis
        diff_mat = RotationMatrixDiffX(in_mat);
    else if (axis == 1) // rotate around y axis
        diff_mat = RotationMatrixDiffY(in_mat);
    else if (axis == 2) // rotate around z axis
        diff_mat = RotationMatrixDiffZ(in_mat);
    return diff_mat;
}

btMatrix3x3 RotationMatrixDiffX(btMatrix3x3 in_mat)
{
    double cos = in_mat[2][1];
    double sin = in_mat[1][1];
    return btMatrix3x3( 0, 0, 0,
                        0, -sin, -cos,
                        0, cos, -sin);
}

btMatrix3x3 RotationMatrixDiffY(btMatrix3x3 in_mat)
{
    double cos = in_mat[0][0];
    double sin = in_mat[0][2];
    return btMatrix3x3(-sin, 0, cos,
                       0, 0, 0,
                       -cos, 0, -sin);
}

btMatrix3x3 RotationMatrixDiffZ(btMatrix3x3 in_mat)
{
    double cos = in_mat[0][0];
    double sin = in_mat[1][0];
    return btMatrix3x3(-sin, -cos, 0,
                       cos, -sin, 0,
                       0, 0, 0);
}

}
