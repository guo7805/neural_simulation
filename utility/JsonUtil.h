#ifndef JSONUTIL_H
#define JSONUTIL_H


#include <iostream>
#include <fstream>
#include <string>
#include <json/json.h>

namespace JsonUtil{

    bool LoadFileIntoJsonValue(std::string file_name, Json::Value& return_value);

}
#endif // JSONUTIL_H
