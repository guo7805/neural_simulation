#ifndef LOGUTIL_H
#define LOGUTIL_H

#include<string>
#include<vector>
#include "Eigen/Dense"
#include "Eigen/StdVector"


class Logger
{
public:
    void AppendLog(Eigen::VectorXd item, std::string log_type);
    void WriteLog(std::string out_filename, std::string choice);
    void WriteToFile(std::string out_filename, std::vector<Eigen::VectorXd> data);
    void ClearLog(std::string log_type);
    bool IsEmpty(std::string log_type);
private:
    std::vector<Eigen::VectorXd> log_torque_;
};


#endif // LOGUTIL_H
