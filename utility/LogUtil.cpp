#include <iostream>
#include <iomanip>
#include <fstream>
#include "utility/LogUtil.h"


void Logger::AppendLog(Eigen::VectorXd item, std::string log_type)
{
    if (log_type=="torque")
        log_torque_.push_back(item);
}


void Logger::WriteLog(std::string out_filename, std::string log_type)
{
    if(log_type=="torque")
        WriteToFile(out_filename, log_torque_);
}

std::ostream& operator<<(std::ostream& os, const Eigen::VectorXd& obj)
{
    // write obj to stream
    int num_dim = obj.size();
    for (int indd=0; indd<num_dim; indd++)
        os << std::fixed << std::setprecision(5) << obj[indd] << " " ;
    return os;
}

void Logger::WriteToFile(std::string out_filename, std::vector<Eigen::VectorXd> data)
{
    std::ofstream ofile;
    ofile.open("./output/"+out_filename);
    if(ofile.is_open())
    {
        for(unsigned int indd=0; indd<data.size(); indd++)
        {
            ofile << data[indd] << std::endl;
        }
    }
    else
    {
        std::cout << "Fail to open the file and cannot write the log to the disk" << std::endl;
    }
    ofile.close();
}

void Logger::ClearLog(std::string log_type)
{
    if(log_type=="torque")
        log_torque_.clear();
}

bool Logger::IsEmpty(std::string log_type)
{
    if(log_type=="torque")
    {
        if(log_torque_.size()==0)
            return true;
        else
            return false;
    }
}
