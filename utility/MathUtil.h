#ifndef MATHUTIL_H
#define MATHUTIL_H

#include<iostream>
#include<algorithm>
#include "btBulletDynamicsCommon.h"
#include "Eigen/Dense"
#include "Eigen/StdVector"

namespace MathUtil
{
    int Clamp(int val, int min_val, int max_val);
    double Clamp(double val, double min_val, double max_val);
    Eigen::VectorXd ConcatVectors(Eigen::VectorXd v1, Eigen::VectorXd v2);
    Eigen::VectorXd ConvertBulletToEigen(btVector3 bullet_vec);
    btMatrix3x3 RotationMatrixDiff(btMatrix3x3 in_mat, int axis);
    btMatrix3x3 RotationMatrixDiffX(btMatrix3x3 in_mat);
    btMatrix3x3 RotationMatrixDiffY(btMatrix3x3 in_mat);
    btMatrix3x3 RotationMatrixDiffZ(btMatrix3x3 in_mat);


    template<class BidiIter>
    BidiIter random_unique(BidiIter begin, BidiIter end, size_t num_random)
    {
        size_t left = std::distance(begin, end);
        while (num_random--)
        {
            BidiIter r = begin;
            std::advance(r, rand()%left);
            std::swap(*begin, *r);
            ++begin;
            --left;
        }
        return begin;
    }
}


#endif // MATHUTIL_H
