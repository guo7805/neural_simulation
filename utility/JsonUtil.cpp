#include "utility/JsonUtil.h"


namespace JsonUtil{

bool LoadFileIntoJsonValue(std::string file_name, Json::Value& return_value)
{
    std::ifstream json_file;
    json_file.open(file_name.c_str());

    //assert(json_file.is_open());
    if(!json_file.is_open())
        std::cout << "Failed to open " << file_name << std::endl;

    Json::Reader reader;
    bool succ = reader.parse(json_file, return_value);
    json_file.close();
    return succ;
}

}
