"""
This module defines a set of layers to be integrated in the network.

                 hyperparameter
Convolution   |  number of output (or channels, or how many filters)
              |  kernel width
              |  stride (default: 1)
Inner Product |  number of output
Pooling       |  kernel width
              |  stride (default: 1)
-----------------------------------------------
ReLU          |  None  
Data (Test)   |  Input Dimension
Data (Train)  |  Input Dimension and Label Dimension
Loss          |  None (Euclidean Type)

Abstract Layer Method:
- output as string, to be called when network outputs

Layer specific methods and property please refer to each layer type
"""

#from caffe import layers as cl
import numpy as np
from random import randint

class Layer(object):    
    # a vector of four dimension to represent four types of layers
    # Convolution:    [1.0, 0.0, 0.0, 0.0]
    # Inner Product:  [0.0, 1.0, 0.0, 0.0]
    # Pooling:        [0.0, 0.0, 1.0, 0.0]
    # ReLU:           [0.0, 0.0, 0.0, 1.0]
    # layer_probablity = np.array([0.0, 0.0, 0.0, 0.0])
    
    def __init__(self):
        pass
    
    
    def set_name(self, layer_index, name=None):
        if name is not None:
            self.name = name
        else:
            self.name = self.layer_type+'L'+str(layer_index)    
    
    def get_name(self):
        return self.name
        
    def get_template_file_name(self):
        template_file_name = 'templates/layers/'+self.layer_type+'.txt'
        return template_file_name
    
    def construct_dict(self):
        raise NotImplementedError("Must override construct_dict in child class")
    
    def adjust_attr_random(self, attr_list=None):
        if attr_list == None:
            attr_keys = self.attr_dict.keys()
        for key in attr_keys:
            num_options = len(self.attr_options_dict[key])
            value_option = randint(0, num_options-1)          
            value = self.attr_options_dict[key][value_option]
            self.attr_dict[key] = value
    
    def output(self, bottom_layer):
        template_file_name = self.get_template_file_name()
        with open(template_file_name, 'r') as template_file:
            template_str=template_file.read()
        info = self.construct_dict(bottom_layer)    
        out_str = template_str.format(**info)
        #out_str = out_str.replace('[', '{')
        #out_str = out_str.replace(']', '}')
        return out_str     
    
class ConvLayer(Layer):
    """Convolution Layer
    
    Attributes:
    - number of output
    - kernel width
    - stride (default value: 1)
    - max number of output
    
    """
    layer_type = 'convolution'
    layer_probablity = np.array([1.0, 0.0, 0.0, 0.0])
    attr_options_dict = {"num_output": [16, 32, 64, 128, 256, 512],
                         "kernel_width": [4, 6, 8, 10, 12, 14, 16]}
    def __init__(self, num_output_in=None, kernel_width_in=None):
        self.attr_dict = {"num_output": num_output_in, 
                          "kernel_width": kernel_width_in}          
        if num_output_in == None and kernel_width_in == None:
            self.adjust_attr_random()
        
       

    
    def construct_dict(self, bottom_layer):
        info = {'name': self.name,
                'bottom': bottom_layer,
                'top': self.name,
                'num_output': self.attr_dict["num_output"],
                'kernel_w': self.attr_dict["kernel_width"]
                }
        return info
        
        
    
    
class InnerLayer(Layer):
    """Inner Product Layer
    
    Attributes:
    - number of output
    - max number of output
    """
    layer_type='innerproduct'
    layer_probablity = np.array([0.0, 1.0, 0.0, 0.0])    
    attr_options_dict = {"num_output": [16, 32, 64, 128, 256, 512]}
    def __init__(self, num_output_in=None):
        self.attr_dict = {"num_output": num_output_in}
        if num_output_in == None:
            self.adjust_attr_random()
    
    def construct_dict(self, bottom_layer):
        info = {'name': self.name,
                'bottom': bottom_layer,
                'top': self.name,
                'num_output': self.attr_dict["num_output"]
                }
        return info

class PoolLayer(Layer):
    """Pooling layer
    
    Attributes:
    - kernel width
    - stride (default value: 1)
    """
    layer_type='pool'
    layer_probablity = np.array([0.0, 0.0, 1.0, 0.0])    
    attr_options_dict = {"kernel_size": [4, 6, 8, 10, 12, 14, 16]}
    def __init__(self, kernel_size_in=None):
        self.attr_dict = {"kernel_size": kernel_size_in}
        if kernel_size_in == None:
            self.adjust_attr_random()
    
    def construct_dict(self, bottom_layer):
        info = {'name': self.name,
                'bottom': bottom_layer,
                'top': self.name,
                'kernel_size': self.attr_dict["kernel_size"]
                }
        return info    

    
class ReLULayer(Layer):
    """ReLU layer
    
    """
    layer_type='relu'
    layer_probablity = np.array([0.0, 0.0, 0.0, 1.0])
    def __init__(self):
        pass
    
    def construct_dict(self, bottom_layer):
        info = {'name': self.name,
                'bottom': bottom_layer,
                'top': self.name,
                }
        return info   

class DataTestLayer(Layer):
    """Data Layer in the case of testing
    
    """
    layer_type='data_test'
    def __init__(self, dim):
        self.input_dim = dim
    
    def set_name(self, layer_index):
        name='data'     
        
    def construct_dict(self, bottom_layer, top_layer):
        info = {'input_dim':self.input_dim}
        return info 

class DataTrainLayer(Layer):
    """Data layer in the case of training
    
    """
    layer_type='data_train'
    def __init__(self, batch_size_in, data_size_in, label_size_in):
        self.batch_size = batch_size_in
        self.data_size = data_size_in
        self.label_size = label_size_in
    
    def set_name(self, layer_index):
        name='data'
    
    def construct_dict(self, bottom_layer, top_layer):
        info = {'batch_size': self.batch_size,
                'data_size': self.data_size,
                'label_size': self.label_size}
        return info     
    
class LossLayer(Layer):
    """Loss Layer
    
    """
    layer_type='euclidean'
    def __init__(self):
        pass
           
    def construct_dict(self, bottom_layer, top_layer):
        info = {}
        return info    