"""
This module completes the neuro evolution

Property:
- index of generation
- number of individuals per generation (should be divided by 2)
- list of individuals per generation
- number of elites
- list of elites per generation
- max number of generations -> evolution termination
- Q value threshold -> evolution termination

Methods:
- initiate the evolution process
- sampling individuals for next generation
  - network mutation (same as later)
  - layer mutation   (same as later)
- evaluate individual fitness
- select top lambda elites
- neuro evolution and generate individuals for next generation
  - network mutation
  - layer mutation
  - network crossover
    - compute network similarity
    - pair networks for crossover
- evaluate individual fitness (repeat the loop)

"""
import numpy as np
import neuralnet as nn



class NeuroEvolution(object):
    index_gen = 0
    num_indivi = 20
    num_elites = 10
    list_indivi = []    
    list_elites = []
    list_q = []
    simi_matrix = None
    max_gen = 500
    success_q_threshold = 100
    best_q = 0
    
    def __init__(self):
        pass
    
    def create_seed_net(self):
        seed_net = nn.NeuralNet(0, 0)
        self.list_indivi.append(seed_net)
        return seed_net
    
    def sample_population(self):
        self.index_gen = self.index_gen + 1
        # clear the list of individual first
        # list of elites are needed to create next generation
        # list of elites are removed when the new elites are selected
        self.list_indivi.clear()
        if len(self.list_elites) == 0:
            raise "No elites are selected"
        self.sample_population_by_crossover(self.list_elites)
        num_in_need = self.num_indivi - len(self.list_indivi)
        if num_in_need > 0:
            self.sample_population_by_mutation(seed_net, num_inter_layer=num_in_need)
        
        
    def sample_population_by_mutation(self, seed_net, num_inter_layer=10, num_intra_layer=10):    
        # clear the list of individual first
        # self.list_indivi.clear()
        ind = 0
        while(ind<num_inter_layer):            
            indivi = self.mutation_inter_layer(seed_net)
            self.list_indivi.append(indivi)
            ind = ind + 1
        ind = 0
        while(ind<num_intra_layer):
            # if call-by-object, will all individuals be the same?
            indivi = self.mutation_intra_layer(seed_net)
            self.list_indivi.append(indivi)
            ind = ind + 1
            
    
    def sample_population_by_crossover(self, list_parents):
        from itertools import combinations
        pairs = combinations(list_parents, 2)
        
        for (net_a, net_b) in pairs:
            self.compute_pair_similarity(net_a, net_b)
            out_net = self.crossover(net_a, net_b)
            self.list_indivi.append(out_net)

    def evaluate_population(self):
        self.list_q = [indivi.kick_simulation() for indivi in self.list_indivi]        
    
    def select_elites(self):
        elites_indices = np.argsort(self.list_q)[::-1][:self.num_elites]
        self.list_elites = [self.list_indivi[index] for index in elites_indices]
        
    
    def mutation_inter_layer(self, net):
        mutated_net = nn.NeuralNet(self.index_gen, len(self.list_indivi))
        mutated_net.set_list_layers(net.get_list_layers())
        mutated_net.adjust_layer_list_random()
        return mutated_net
    
    def mutation_intra_layer(self, net):        
        mutated_net = nn.NeuralNet(self.index_gen, len(self.list_indivi))
        mutated_net.set_list_layers(net.get_list_layers())
        mutated_net.adjust_layer_attr_random()
        return mutated_net
    
    def crossover(self, net_a, net_b):
        layer_warp_a = net_a.get_warp_probability()
        layer_warp_b = net_b.get_warp_probability()
        layer_output = (layer_warp_a + layer_warp_b)/2
        #layer_output_pool = np.zeros(layer_output.shape)
        #max_indices = np.argmax(layer_output, axis=0)
        #col_indices = range(layer_output.shape[1])
        #layer_output_pool[max_indices, col_indices] = 1
        output_list_layers = []
        for col_ind, layer_prob in enumerate(layer_output.T):
            layer_prob_a = layer_warp_a[:, col_ind]
            layer_prob_b = layer_warp_b[:, col_ind]
            select_a = self.select_one_parent(layer_prob, layer_prob_a, layer_prob_b)
            if select_a == True:
                layer = self.select_layer_from_parent(net_a, col_ind)
            else:
                layer = self.select_layer_from_parent(net_b, col_ind)
            output_list_layers.append(layer)
        output_net = nn.NeuralNet(self.index_gen, len(self.list_indivi))
        output_net.set_list_layers(output_list_layers)
        return output_net
    
    def select_one_parent(self, layer, layer_a, layer_b):
        # check layer is closer to a or b
        # return True if a is selected, False if b
        diff_a = layer - layer_a
        diff_b = layer - layer_b
        if (sum(diff_a**2) < sum(diff_b**2)):
            return True
        else:
            return False
        
    def select_layer_from_parent(self, net, warp_index):
        # pick the corresponding layer from the network
        # note that the layer index is the warped index, so maybe beyond the network depth
        # in this case, should use interpolation
        layer = net.select_layer_via_warp_index(warp_index)
        return layer
    
    def compute_population_similarity(self):
        indr = 0
        indc = 0
        num_indivi = len(self.list_indivi)
        self.simi_matrix = np.zeros(num_indivi, num_indivi)
        # fill in the upper triangular matrix
        while indr < num_indivi:
            indc = indr
            while indr < num_indivi:
                if indr == indc:
                    simi_matrix[indr, indc] = 1.0
                else:
                    net_a = self.list_indivi[indr]
                    net_b = self.list_indivi[indc]
                    simi_matrix[indr, indc] = self.compute_pair_similarity(net_a, net_b)
                indc = indc + 1
            indr = indr + 1
        return self.simi_matrix    
    
    def compute_pair_similarity(self, net_a, net_b):
        depth_a = net_a.get_network_depth()
        depth_b = net_b.get_network_depth()
        
        if(depth_a>depth_b):
            warp_length = depth_a            
        else:
            warp_length = depth_b
        mat_a = net_a.warp_layer_probablity(warp_length)            
        mat_b = net_b.warp_layer_probablity(warp_length)      
        net_similarity = self.compute_matrix_similarity(mat_a, mat_b)
        return net_similarity
    
    def compute_matrix_similarity(self, mat_a, mat_b):
        if (mat_a.shape != mat_b.shape):
            raise "Mismatch matrix Shape when computing the similarity"
        mat_diff = mat_a - mat_b
        mat_ones = np.ones(mat_diff.shape)
        mat_sum = np.sum(np.square(mat_ones-mat_diff))
        return mat_sum
        
    
    def pair_indivi_with_similarity(self):
        pass
    
    
    def check_termination(self):
        if (self.index_gen>self.max_gen) or (self.best_q>self.success_q_threshold):
            return True
        else:
            return False
        
    def output_generation(self, elites=False):
        if elites:
            nets = self.list_elites
        else:
            nets = self.list_indivi
        for net in nets:
            net.output_deploy_network()
            net.output_solver_network()            
            