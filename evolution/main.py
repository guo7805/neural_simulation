from layers import ConvLayer, InnerLayer
import neuroevolution as ne
import neuralnet as nn



def main():
    neuro_evo = ne.NeuroEvolution()
    seed_net = neuro_evo.create_seed_net()    
    neuro_evo.sample_population_by_mutation(seed_net, num_inter_layer=10, num_intra_layer=10)
    neuro_evo.output_generation()
    while not neuro_evo.check_termination():
        neuro_evo.evaluate_population()
        neuro_evo.select_elites()
        neuro_evo.sample_population()

if __name__ == '__main__':
    #test_crossover()   
    main()