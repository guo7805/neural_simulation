"""
This module defines the class of neural net.

Property:
- list of layers
- list of layer probablity
- max number of layer

Methods:
- construct a network from a proto file
- output the network to a proto file
- add/delete a layer from a net
- adjust the layer hyperparameter by calling the member function of a layer
- warping a layer to a specified length
- run the simulation, and collect the fitness value
"""

#import caffe
#from caffe import layers as cl
import numpy as np
from random import randint
from layers import ConvLayer, InnerLayer, PoolLayer, ReLULayer,\
                   LossLayer, DataTestLayer, DataTrainLayer


class NeuralNet(object):
    # list_layers = []
    # layer_probablity = []
    # layer_probablity_warp = []
    # ind_gen = None  # index of generation
    # ind_ind = None  # index of individual
    # folder_name = None # folder to store the relevant files for this individual    
    min_num_layers = 3
    max_num_layers = 20
    dynamic_layers = [ConvLayer, InnerLayer, PoolLayer]
    
    def __init__(self, ind_gen_in, ind_ind_in, is_seed=False):
        import os
        
        self.list_layers = []
        self.layer_probablity = []
        self.layer_probablity_warp = []
        
        self.ind_gen = ind_gen_in
        self.ind_ind = ind_ind_in        
        if ((ind_gen_in==0) and (ind_ind_in==0)) or is_seed:
            self.create_seed_net()
        else:
            self.create_empty_net()
        self.folder_name = self.get_folder_name()
        os.mkdir(self.folder_name)
        
        
    
    def get_network_depth(self):
        return len(self.list_layers)
    
    def get_warp_probability(self):
        if len(self.layer_probablity_warp)==0:
            raise "Layer probablity matrix after warping is not initialized"
        return self.layer_probablity_warp
    
    def get_folder_name(self):
        return "population/"+str(self.ind_gen)+"_"+str(self.ind_ind)
    
    def get_network_name(self, is_deploy):
        if is_deploy:
            return "actor_deploy_"+str(self.ind_gen)+"_"+str(self.ind_ind)
        else:
            return "actor_solver_"+str(self.ind_gen)+"_"+str(self.ind_ind)
    
    def get_list_layers(self):
        # make a copy, so this internal doesn't get affected
        import copy as cp
        return cp.deepcopy(self.list_layers)
    
    def set_list_layers(self, list_layers_in):
        self.list_layers = list_layers_in
    
    def create_seed_net(self):
        #data_layer = DataTestLayer(self.input_dim)
        conv_L1 = ConvLayer(16, 8)
        relu_L1 = ReLULayer()
        conv_L2 = ConvLayer(32, 4)
        relu_L2 = ReLULayer()
        conv_L3 = ConvLayer(32, 4)
        relu_L3 = ReLULayer()        
        inner_L4 = InnerLayer(64)
        relu_L4 = ReLULayer()
        #inner_L5 = InnerLayer(256)
        self.list_layers = [conv_L1, relu_L1, 
                            conv_L2, relu_L2, conv_L3, relu_L3,
                            inner_L4, relu_L4]
    

        
    
    def create_empty_net(self):
        pass
    
    def select_layer_via_warp_index(self, warp_index):
        if len(self.layer_probablity_warp) == 0:
            raise "Layer probablity matrix after warping is not initialized"
        rel_pose = warp_index / self.layer_probablity_warp.shape[1] * len(self.list_layers)
        pose = int(round(rel_pose))
        if pose > (len(self.list_layers)-1):
            pose = len(self.list_layers) - 1
        return self.list_layers[pose]
        

    def insert_layer(self, layer, layer_index):
        # if the inserted position is a RELU layer, then move to the next position
        if (layer_index>=len(self.list_layers)):
            raise "the inserted position is beyond the current depth of the network"
        #if type(self.list_layers[layer_index+1]==ReLULayer):
        #    layer_index = layer_index + 1
        self.list_layers.insert(layer_index, layer)
        # if (type(layer) == ConvLayer) or (type(layer) == InnerLayer):
        #    if type(self.list_layers[layer_index+1]) != ReLULayer:
        #        relu = ReLULayer()
        #        self.list_layers.insert(layer_index+1, relu)
    
    def delete_layer(self, layer_index):
        del self.list_layers[layer_index]
        # check if next layer is RELU
        # if True, delete next RELU layer
        # if type(self.list_layers[layer_index]) == ReLULayer:
        #     del self.list_layers[layer_index]
    
    def adjust_layer_list_random(self):
        target_num_layers = randint(self.min_num_layers, self.max_num_layers)
        diff_num_layers = target_num_layers - len(self.list_layers)
        if (diff_num_layers>0):
            while(diff_num_layers!=0):
                self.insert_layer_random_position()
                diff_num_layers = diff_num_layers - 1
        else:
            while(diff_num_layers!=0):
                self.delete_layer_random_position()
                diff_num_layers = diff_num_layers + 1
    
    def insert_layer_random_position(self):
        layer = self.generate_random_layer()
        layer_index = randint(0, len(self.list_layers)-1)
        self.insert_layer(layer, layer_index)

    def generate_random_layer(self):
        layer_type = randint(0, len(self.dynamic_layers)-1)
        layer = self.dynamic_layers[layer_type]()
        return layer
        
    
    def delete_layer_random_position(self):
        layer_index = randint(0, len(self.list_layers)-1)
        self.delete_layer(layer_index)
        
    
    
    def adjust_layer_attr_random(self, layer_index=None):
        # layer = self.list_layers[layer_index]
        while True:
            layer_index = randint(0, self.get_network_depth()-1)
            layer = self.list_layers[layer_index]            
            if type(layer) in self.dynamic_layers:
                break
        layer.adjust_attr_random()
        
    
    #def adjust_layer(self, layer_index, attr_name, attr_value):
    #    setattr(self.list_layers[layer_index], attr_name, attr_value)
    
    def kick_simulation(self):
        import os
        import subprocess
        os.environ['LD_LIBRARY_PATH'] = "/home/guoshihui/Desktop/neural/neural_insect/external/caffe/build/lib/debug/"        
        exec_file_name = "bin/Debug/neural_optimizer"
        scenario_file_name = "evolution/" + self.output_scenario_args("templates/args/train_hexapod_skeleton.scenario")
        network_file_name = "evolution/" + self.output_network_args("templates/args/train_hexapod_skeleton.net")
        cmd = exec_file_name + ' ' + scenario_file_name + ' ' + network_file_name
        print(cmd)
        # This is not good, since you change the working directory, other paths are polluted
        # os.chdir("..")
        # os.system(cmd)       
        #p = subprocess.Popen([exec_file_name, scenario_file_name+' '+network_file_name], cwd="..", shell=True)
        #p.wait()        
        # This is to fake the output of each individual, as the fitness
        # Should be the output from the simulation
        q = randint(0, 50)
        return q
    
    def output_scenario_args(self, scenario_args_template):
        with open(scenario_args_template, 'r') as skeleton_file:
            skeleton_str=skeleton_file.read()
        scenario_argfile_name = self.folder_name+"/train.scenario"
        with open(scenario_argfile_name, 'w') as out_file:
            out_file.write(skeleton_str)
        return scenario_argfile_name
    
    def output_network_args(self, network_args_template):
        with open(network_args_template, 'r') as skeleton_file:
            skeleton_str=skeleton_file.read()        
        network_argfile_name = self.folder_name + "/train.net"
        info = {"actor": "evolution/"+self.folder_name+"/actor.txt",
                "actor_solver": "evolution/"+self.folder_name+"/actor_solver.prototxt"}
        output_str = skeleton_str.format(**info)
        with open(network_argfile_name, 'w') as out_file:
            out_file.write(output_str)
        return network_argfile_name
    
    def warp_layer_probablity(self, warp_length):
        x = np.linspace(0, len(self.list_layers)-1, warp_length)
        xp = np.linspace(0, len(self.list_layers)-1, len(self.list_layers))
        yp_mat = self.construct_layer_probability_matrix()
        yp_mat_interp = self.interpolate_layer_probability_matrix(x, xp, yp_mat)
        self.layer_probablity_warp = yp_mat_interp
        return yp_mat_interp
        
    def construct_layer_probability_matrix(self):
        prob_list = []
        for layer in self.list_layers:
            prob_list.append(layer.layer_probablity)
        prob_mat = np.array(prob_list)
        self.layer_probablity = np.transpose(prob_mat)
        return self.layer_probablity
    
    def interpolate_layer_probability_matrix(self, x, xp, yp):
        from scipy.interpolate import interp1d
        f = interp1d(xp, yp)
        interp_mat = f(x)
        return interp_mat
    
    def fresh_layer_name(self):
        for layer_index, layer in enumerate(self.list_layers):
            layer.set_name(layer_index)
        
    def output_network(self, network_name, output_file_name, skeleton_file_name):
        output_str = ""
        self.fresh_layer_name()
        for layer_index, layer in enumerate(self.list_layers):
            if layer_index == 0:
                bottom_layer = "data"
            else:
                bottom_layer = self.list_layers[layer_index-1].get_name()                 
            output_str = output_str + layer.output(bottom_layer)
        with open(skeleton_file_name, 'r') as skeleton_file:
            skeleton_str=skeleton_file.read()
        last_bottom = self.list_layers[-1].get_name()            
        output_str = skeleton_str.format(**{"network_name": network_name, 
                                            "network_body": output_str,
                                            "last_bottom": last_bottom})
        #output_str = output_str.replace('[', '{')
        #output_str = output_str.replace(']', '}')
        with open(output_file_name, "w") as out_file:
            out_file.write(output_str)
    
    def output_deploy_network(self):
        network_name = self.get_network_name(is_deploy=True)
        output_file_name = self.folder_name+"/actor.txt"
        skeleton_file_name="templates/network/actor_network_skeleton.txt"
        self.output_network(network_name, output_file_name, skeleton_file_name)
    
    def output_solver_network(self):
        network_name = self.get_network_name(is_deploy=False)
        solver_network_filename = self.folder_name+"/actor_solver.txt"
        skeleton_file_name="templates/network/actor_solver_network_skeleton.txt"
        self.output_network(network_name, solver_network_filename, skeleton_file_name)
        proto_skeleton_filename = "templates/network/actor_solver_skeleton.prototxt"
        proto_output_filename = self.folder_name+"/actor_solver.prototxt"
        self.output_solver_proto(proto_skeleton_filename, proto_output_filename, "evolution/"+solver_network_filename)
        
        
    def output_solver_proto(self, proto_skeleton_filename, proto_output_filename, solver_network_path):
        with open(proto_skeleton_filename, 'r') as proto_file:
            out_str = proto_file.read()
        out_str = out_str.format(**{"network_path":solver_network_path})
        with open(proto_output_filename, 'w') as proto_out_file:
            proto_out_file.write(out_str)