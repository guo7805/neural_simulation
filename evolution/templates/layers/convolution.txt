layer {{
  name: "{name}"
  type: "Convolution"
  bottom: "{bottom}"
  top: "{top}"
  param {{
    lr_mult: 1
  }}
  param {{
    lr_mult: 2
  }}
  convolution_param {{
    num_output: {num_output}
    kernel_h: 1
	kernel_w: {kernel_w}
    stride: 1
    weight_filler {{
      type: "xavier"
    }}
    bias_filler {{
      type: "constant"
    }}
  }}
}}


