# My project's README


# Axis Direction
When the character is at its default position, it faces +z, and the up direction is +y

# Folder Structure


# Dependency

Ubuntu 16.04
GCC 5.4.0

## System-wide dependency

These libraries are mostly caffe-related. If you got caffe running before, you probably got most (if not all) of them.

    sudo apt-get install libboost-all-dev libgoogle-glog-dev libgflags-dev libprotobuf-dev protobuf-compiler 
    sudo apt-get install libatlas-base-dev libhdf5-serial-dev liblmdb-dev libleveldb-dev libopencv-dev

In addition, you need to install three OPENGL libraries first: GL, GLU, GLUT (ubuntu) for the simulation project (for visualization of character animation).

    sudo apt-get install libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev



## Project specific dependency
The libraries (header files or compiled archives) are stored under external/, while the compressed library (original) is stored at external/compressed_external

Bullet (Version: 2.82)
Both debug and release versions are provided. They are to build debug/release target of the project.

JsonCPP (Version: 1.8.1 f26edb05e53d137fa5608d3f7d5dd50ab8d97235 Updated on 2017/07/17)
JsonCPP used to read the configuration files

Caffe (Version: 4efdf7ee49cffefdd7ea099c00dc5ea327640f04 Updated on 2017/06/21)
The deep learning framework. Recommend to follow the install instruction on [Caffe homepage](http://caffe.berkeleyvision.org/install_apt.html).


However, minor revisions are made to three files: caffe.proto, MemoryDataLayer.hpp, MemoryDataLayer.cpp. These revisions are made to enable the input of multi-dimension label in the memory data layer.


Protoc 2.6.1
When the protoc on your system is older or newer than this version, you have to recompile the Caffe library. Otherwise you will get errors.


Eigen (Version: )




# Build Instructions


Under the tools/cbp2make folder, we provide the executable file to convert the codeblocks project into makefile. Two pre-generated makefiles are provided. Copy them to the project level and you can use:

    make debug
    make release

to compile these projects in console mode. This is useful if the optimizer needs to be compiled and run in a server with no X.


# Running Instructions
To run the program under different situation, use the setting files under the folder args/

    bin/Release/neural_insect args/hexapod/hexapod.txt
    bin/Release/neural_optimizer args/hexapod/train_hexapod.scenario args/hexapod/train_hexapod.net


If there is an error message reporting that libcaffe.so cannot be found, remember to add the path to LD_LIBRARY_PATH:

    export LD_LIBRARY_PATH=/home/caffe/Documents/neural_insect/external/caffe/lib/debug/

You can use ldd to check the requirements of dynamic libraries of the executable.

