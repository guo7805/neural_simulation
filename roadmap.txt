# This includes the roadmap to the whole project

The code style follows the Google Style for C++:
https://google.github.io/styleguide/cppguide.html


# The ultimate goal is to develop a simulation platform for simulating and analysing the motion of different characters.

## Phase 1

PD servos
Evolving networks

### Milestone 1
Connect 


### Milestone 2


### Milestone 3




## Phase 2

Nonlinear Muscle
Spiking Neurons


## Phase 3

Challenging terrains
