#include "learning/neuralNet.h"
#include "utility/FileUtil.h"


const std::string INPUT_LAYER_NAME = "data";


NeuralNet::NeuralNet()
{

}

NeuralNet::~NeuralNet()
{

}


void NeuralNet::Clear()
{
    //mNet_->reset();
    //mSolver.reset();
    mValidModel = false;

    mInputOffset.resize(0);
    mInputScale.resize(0);
    mOutputOffset.resize(0);
    mOutputScale.resize(0);
}


void NeuralNet::LoadNet(const std::string& net_file)
{
    if(net_file!="")
    {
        mNet_ = new caffe::Net<double>(net_file, caffe::TEST);
    }
}

void NeuralNet::LoadModel(const std::string& model_file)
{
    if(model_file!="")
    {
        if(HasNet())
        {
            mNet_->CopyTrainedLayersFromHDF5(model_file);
            //LoadScale(GetOffsetScaleFile(model_file));
            SyncSolverParams();

            mValidModel = true;
        }
        else if(HasSolver())
        {
            auto net = GetTrainNet();
            net->CopyTrainedLayersFromHDF5(model_file);
            //LoadScale(GetOffsetScaleFile(model_file));
            SyncNetParams();

            mValidModel = true;
        }
    }
}

void NeuralNet::OutputModel(const std::string& out_file) const
{
    if (HasNet())
    {
        mNet_->ToHDF5(out_file);
//        std::string scale_file = GetOffsetScaleFile(out_file);
//        WriteOffsetScale(scale_file);
    }
    else
    {
        printf("No valid net to output\n");
    }
}

void NeuralNet::InitModel()
{

}


void NeuralNet::LoadSolver(const std::string& solver_file)
{
    if (solver_file != "")
    {
        mSolverFile_ = solver_file;
        caffe::SolverParameter param;
        caffe::ReadProtoFromTextFileOrDie(solver_file, &param);
        caffe::Caffe::set_mode(caffe::Caffe::CPU);
        caffe::SolverParameter_SolverType type = param.solver_type();

        switch (type)
        {
        case caffe::SolverParameter_SolverType_SGD:
            mSolver_ = new caffe::SGDSolver<double>(param);
            break;
        case caffe::SolverParameter_SolverType_NESTEROV:
            mSolver_ = new caffe::NesterovSolver<double>(param);
            break;
        case caffe::SolverParameter_SolverType_ADAGRAD:
            mSolver_ = new caffe::AdaGradSolver<double>(param);
            break;
        case caffe::SolverParameter_SolverType_RMSPROP:
            mSolver_ = new caffe::RMSPropSolver<double>(param);
            break;
        case caffe::SolverParameter_SolverType_ADADELTA:
            mSolver_ = new caffe::AdaDeltaSolver<double>(param);
            break;
        case caffe::SolverParameter_SolverType_ADAM:
            mSolver_ = new caffe::AdamSolver<double>(param);
            break;
            //default:
            //    LOG(FATAL) << "Unknown SolverType: " << type;
        }
    }
}

void NeuralNet::LoadScale(const std::string& scale_file)
{

}

int NeuralNet::GetInputSize() const
{
    int input_size = 0;
    if (HasNet())
    {
        input_size = mNet_->input_blobs()[0]->count();
    }
    else if (HasSolver())
    {

    }
    return input_size;
}

int NeuralNet::GetOutputSize() const
{
    int output_size = 0;
    if (HasNet())
    {
        output_size = mNet_->output_blobs()[0]->count();
    }
    else if (HasSolver())
    {

    }

    return output_size;
}

bool NeuralNet::HasNet() const
{
    return mNet_!=nullptr;
}

bool NeuralNet::HasSolver() const
{
    return mSolver_!=nullptr;
}

const std::vector<caffe::Blob<double>*>& NeuralNet::GetParams() const
{
	if (HasNet())
	{
		return mNet_->learnable_params();
	}
	else
	{
		auto net = GetTrainNet();
		return net->learnable_params();
	}
}

void NeuralNet::CopyModelFrom(const NeuralNet& other)
{
	CopyParams(other.GetParams(), GetParams());

//	mInputOffset = other.GetInputOffset();
//	mInputScale = other.GetInputScale();
//	mOutputOffset = other.GetOutputOffset();
//	mOutputScale = other.GetOutputScale();

//	SyncSolverParams();
//	mValidModel = true;
}

void NeuralNet::CopyModel(const caffe::Net<double>& src, caffe::Net<double>& dst)
{
	const auto& src_params = src.learnable_params();
	const auto& dst_params = dst.learnable_params();
	CopyParams(src_params, dst_params);
}

void NeuralNet::CopyParams(const std::vector<caffe::Blob<double>*>& src_params, const std::vector<caffe::Blob<double>*>& dst_params)
{
	int num_blobs = static_cast<int>(src_params.size());
	for (int b = 0; b < num_blobs; ++b)
	{
		auto src_blob = src_params[b];
		auto dst_blob = dst_params[b];

		auto src_blob_data = src_blob->cpu_data();
		auto dst_blob_data = dst_blob->mutable_cpu_data();
		int src_blob_count = src_blob->count();
		int dst_blob_count = dst_blob->count();

		if (src_blob_count == dst_blob_count)
		{
			std::memcpy(dst_blob_data, src_blob_data, src_blob_count * sizeof(double));
		}
		else
		{
			assert(false); // param size mismatch
		}
	}
}



void NeuralNet::SyncNetParams()
{
	if (HasSolver() && HasNet())
	{
		CopyModel(*GetTrainNet(), *mNet_);
	}
}

void NeuralNet::SyncSolverParams()
{
	if (HasSolver() && HasNet())
	{
		CopyModel(*mNet_, *GetTrainNet());
	}
}

void NeuralNet::NormalizeInput(Eigen::VectorXd& x) const
{
    if (ValidOffsetScale())
    {
        assert(x.size() == mInputOffset.size());
        assert(x.size() == mInputScale.size());
        x += mInputOffset;
        x = x.cwiseProduct(mInputScale);
    }
}


void NeuralNet::UnnormalizeOutput(Eigen::VectorXd& y) const
{
    if (ValidOffsetScale())
    {
        assert(y.size() == mOutputOffset.size());
        assert(y.size() == mOutputScale.size());
        y = y.cwiseQuotient(mOutputScale);
        y -= mOutputOffset;
    }
}


std::string NeuralNet::GetOffsetScaleFile(const std::string& model_file)
{
    std::string scale_file = model_file;
    scale_file = FileUtil::RemoveExtension(model_file);
    scale_file += "_scale.txt";
    return scale_file;
}


bool NeuralNet::ValidOffsetScale() const
{
    return mInputOffset.size() > 0 && mInputScale.size() > 0
           && mOutputOffset.size() > 0 && mOutputScale.size() > 0;
}

Eigen::VectorXd NeuralNet::Evaluate(Eigen::VectorXd x)
{
    Eigen::VectorXd out_y;

    const int input_size = GetInputSize();

    caffe::Blob<double> blob(1, 1, 1, input_size);
    double* blob_data = blob.mutable_cpu_data();

    Eigen::VectorXd norm_x = x;
    NormalizeInput(norm_x);

    for (int i = 0; i < blob.count(); ++i)
    {
        blob_data[i] = norm_x[i];
    }

    //double loss = 0;
    const std::vector<caffe::Blob<double>*>& input_blobs = mNet_->input_blobs();
    input_blobs[0]->CopyFrom(blob);
    const std::vector<caffe::Blob<double>*>& result_arr = mNet_->Forward();

    out_y = FetchOutput(result_arr);
    return out_y;
}

Eigen::VectorXd NeuralNet::FetchOutput(const std::vector<caffe::Blob<double>*>& results_arr) const
{
    Eigen::VectorXd out_y;
    const caffe::Blob<double>* result = results_arr[0];
    const double* result_data = result->cpu_data();

    const int output_size = GetOutputSize();
    //assert(result->count() == output_size);
    out_y.resize(output_size);

    for (int i = 0; i < output_size; ++i)
    {
        out_y[i] = result_data[i];
    }

    UnnormalizeOutput(out_y);

    return out_y;
}

void NeuralNet::Train(const Problem& prob)
{
    LoadTrainData(prob.mX, prob.mY);
    int num_batches = prob.mX.rows();
    mSolver_->Step(num_batches);
}

void NeuralNet::LoadTrainData(const Eigen::MatrixXd& X, const Eigen::MatrixXd& Y)
{
    auto data_layer = GetTrainDataLayer();

    int num_batches = 1;
    int batch_size = GetBatchSize();

    int num_data = num_batches * batch_size;
    int data_dim = static_cast<int>(X.cols());
    int label_dim = static_cast<int>(Y.cols());

    std::vector<double> data(num_data * data_dim);
    std::vector<double> labels(num_data * label_dim);

    for (int i = 0; i < num_data; ++i)
    {
        auto curr_data = X.row(i);
        auto curr_label = Y.row(i);

        for (int j = 0; j < data_dim; ++j)
        {
            double val = curr_data[j];
            //if (ValidOffsetScale())
            //{
            //    val += mInputOffset[j];
            //    val = val * mInputScale[j];
            //}
            data[i * data_dim + j] = val;
        }

        for (int j = 0; j < label_dim; ++j)
        {
            double val = curr_label[j];
            //if (ValidOffsetScale())
            //{
            //    val += mOutputOffset[j];
            //    val = val * mOutputScale[j];
            //}
            labels[i * label_dim + j] = val;
        }
    }

    data_layer->AddData(data, labels);

}

boost::shared_ptr<caffe::MemoryDataLayer<double>> NeuralNet::GetTrainDataLayer() const
{
    auto train_net = mSolver_->net();
    const std::string& data_layer_name = GetInputLayerName();
    auto data_layer = boost::static_pointer_cast<caffe::MemoryDataLayer<double>>(train_net->layer_by_name(data_layer_name));
    return data_layer;
}

boost::shared_ptr<caffe::Net<double>> NeuralNet::GetTrainNet() const
{
    if (HasSolver())
    {
        return mSolver_->net();
    }
    return nullptr;
}

const std::string& NeuralNet::GetInputLayerName() const
{
    return INPUT_LAYER_NAME;
}


int NeuralNet::GetBatchSize() const
{
    int batch_size = 0;
    auto data_layer = GetTrainDataLayer();
    batch_size = data_layer->batch_size();
    return batch_size;
}
