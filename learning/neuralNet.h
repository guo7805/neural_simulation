#ifndef NEURALNET_H
#define NEURALNET_H

#include <caffe/net.hpp>
#include <caffe/caffe.hpp>
#include <caffe/layers/memory_data_layer.hpp>
#include <caffe/sgd_solvers.hpp>
#include "Eigen/Dense"
#include "Eigen/StdVector"

struct Problem
{
    Eigen::MatrixXd mX;
    Eigen::MatrixXd mY;

    bool HasData() const;
};


class NeuralNet
{
public:
	NeuralNet();
	virtual ~NeuralNet();
    virtual void Clear();
	virtual void LoadNet(const std::string& net_file);
	virtual void LoadModel(const std::string& model_file);
	virtual void LoadSolver(const std::string& solver_file);
	virtual void LoadScale(const std::string& scale_file);
	virtual void InitModel();


    virtual int GetInputSize() const;
	virtual int GetOutputSize() const;
	virtual void SyncSolverParams();
	virtual void CopyModelFrom(const NeuralNet& other);
	virtual void CopyModel(const caffe::Net<double>& src, caffe::Net<double>& dst);
    virtual void CopyParams(const std::vector<caffe::Blob<double>*>& src_params, const std::vector<caffe::Blob<double>*>& dst_params);
    const std::vector<caffe::Blob<double>*>& GetParams() const;
	virtual void SyncNetParams();
	virtual void NormalizeInput(Eigen::VectorXd& x) const;
	virtual void UnnormalizeOutput(Eigen::VectorXd& y) const;
    virtual std::string GetOffsetScaleFile(const std::string& model_file);

    void OutputModel(const std::string& out_file) const;

    Eigen::VectorXd Evaluate(Eigen::VectorXd x);
    Eigen::VectorXd FetchOutput(const std::vector<caffe::Blob<double>*>& results_arr) const;
	bool HasNet() const;
	bool HasSolver() const;
	bool ValidOffsetScale() const;

    void Train(const Problem& prob);
	void LoadTrainData(const Eigen::MatrixXd& X, const Eigen::MatrixXd& Y);
	virtual boost::shared_ptr<caffe::MemoryDataLayer<double>> GetTrainDataLayer() const;
	virtual boost::shared_ptr<caffe::Net<double>> GetTrainNet() const;
	const std::string& GetInputLayerName() const;
	int GetBatchSize() const;

private:
    bool mValidModel;
	//bool mAsync;

	caffe::Net<double>* mNet_;
	caffe::SGDSolver<double>* mSolver_;
	std::string mSolverFile_;

	Eigen::VectorXd mInputOffset;
	Eigen::VectorXd mInputScale;
	Eigen::VectorXd mOutputOffset;
	Eigen::VectorXd mOutputScale;

};


#endif // NEURALNET_H
