#ifndef NEURALNETLEARNER_H
#define NEURALNETLEARNER_H

#include "learning/neuralNet.h"

class NeuralNetQLearner
{
public:
    NeuralNetQLearner();
    ~NeuralNetQLearner();
    void Initialize();
    void ExploreAction();
    void Observe();
    void StoreTransition();
private:
    NeuralNet* critic_;
    NeuralNet* actor_;
};


#endif // NEURALNETLEARNER_H
