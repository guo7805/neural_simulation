#ifndef NEURALNETQTRAINER_H
#define NEURALNETQTRAINER_H


class NeuralNetQTrainer
{
public:
    NeuralNetQTrainer();
    ~NeuralNetQTrainer();
    void Initialize();
    void UpdateCritic();
    void UpdateActor();
    void UpdateTarget();
private:
    NeuralNet* critic_target_;
    NeuralNet* actor_target_;
};



#endif // NEURALNETQTRAINER_H
