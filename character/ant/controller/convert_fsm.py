import pandas as pd

template_file_name = 'ant.ctrl.template'
fsm_file_name = "fsm.xlsx"
out_file_name = "ant.ctrl"

dof_list = ['UpHip', 'Hip', 'Knee', 'Ankle'] 
states_list = ['S1', 'S2', 'S3', 'S4']

states_dict = {}

for state in states_list:
    states_dict[state] = [dof+state for dof in dof_list]

fsm = pd.read_excel(fsm_file_name, skiprows=1, index_col=0)

info_dict = {}

for state in states_dict.keys():
    state_str = ''
    fsm_s = fsm[states_dict[state]]
    for index, row in fsm_s.iterrows():
        row_str = ','.join(str(e) for e in list(row))
        state_str = state_str + row_str
        if index != fsm_s.index[-1]:
            state_str = state_str + ','
    info_dict[state] = state_str
    

with open(template_file_name, 'r') as in_file:
    template_str = in_file.read()

out_str = template_str.format(**info_dict)

with open(out_file_name, 'w') as out_file:   
    out_file.write(out_str)