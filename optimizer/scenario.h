#ifndef SCENARIO_H
#define SCENARIO_H


#include <string>
#include <iostream>
#include "LinearMath/btAlignedObjectArray.h"
#include "simulation/terrainFlat.h"
#include "simulation/character.h"
#include "learning/neuralNet.h"
#include "utility/JsonUtil.h"

class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;


class Scenario
{
public:
    Scenario();
    ~Scenario();
    Scenario(std::string arg_file);
	void	initPhysics();
	void    initTerrain();
	void    initCharacter(const Json::Value& args);
	void    initCharacterNet(NeuralNet* net);
	Eigen::VectorXd    Observe();

	bool IsTerminated();
	void Reset();

	void	exitPhysics();
	bool    Evaluate(const Eigen::VectorXd action);
    void    UpdateSimulationStep(double timeStep);

    double    CalcReward();
private:
	//keep the collision shapes, for deletion/cleanup
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;
	btBroadphaseInterface*	m_broadphase;
	btCollisionDispatcher*	m_dispatcher;
	btConstraintSolver*	m_solver;
	btDefaultCollisionConfiguration* m_collisionConfiguration;
    btDynamicsWorld*		m_dynamicsWorld;
    Terrain*    terrain_;
    Character*  charac_;
    Eigen::VectorXd target_vel;
    Eigen::VectorXd init_position;
    Eigen::VectorXd final_position;
};



#endif // SCENARIO_H
