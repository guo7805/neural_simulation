#include <iostream>
#include <fstream>
#include "optimizerQLearning.h"
#include "scenario.h"
#include "utility/JsonUtil.h"

std::string record_file_name = "output/record/output.txt";

int main(int argc, char** argv)
{
    std::ofstream record_file;
    record_file.open (record_file_name);

    Scenario* sce = new Scenario(argv[1]);
    OptimizerQLearning* opt = new OptimizerQLearning(argv[2]);

    NeuralNet* actor_net = opt->GetActor();
    sce->initCharacterNet(actor_net);

    Eigen::VectorXd state_begin, state_end, action_std, action, next_action;
    double reward, predict_total_reward, predict_next_total_reward, predict_total_reward_std;

    std::vector<ExpTuple> mini_batch;

    bool learning_done=false;
    while(!learning_done)
    {
        // one loop means one locomotion cycle
        state_begin = sce->Observe();
        action_std = opt->DecideAction(state_begin);
        action = action_std + opt->ActionNoise(action_std);
        bool episide_done = sce->Evaluate(action);
        state_end = sce->Observe();
        reward = sce->CalcReward();
        predict_total_reward = opt->PredictReward(state_begin, action);
        predict_total_reward_std = opt->PredictReward(state_begin, action_std);

        Eigen::IOFormat CleanFmt(4, 0, " ", " ");
        record_file << reward << " " << state_begin.format(CleanFmt) << state_end.format(CleanFmt) << std::endl;

        next_action = opt->DecideAction(state_end);
        predict_next_total_reward = opt->PredictReward(state_end, next_action);

        ExpTuple exp(state_begin, action_std, action, state_end,
                     reward, predict_total_reward, predict_next_total_reward, predict_total_reward_std);
        opt->AppendExpTuple(exp);
        if(opt->CheckEnoughReplay())
        {
            opt->SampleMiniBatch(mini_batch);
            opt->Update(mini_batch);
        }

        if(episide_done)
            sce->Reset();
        learning_done = opt->CheckTermination();
    }
    opt->Output();
    record_file.close();
    return 0;
}
