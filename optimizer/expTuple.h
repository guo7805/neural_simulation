#ifndef EXPTUPLE_H
#define EXPTUPLE_H

#include "Eigen/Dense"
#include "Eigen/StdVector"

class ExpTuple
{
public:

	ExpTuple();
    ExpTuple(Eigen::VectorXd stateBeg, Eigen::VectorXd Action_std, Eigen::VectorXd Action, Eigen::VectorXd stateEnd,
             double reward, double predict_total_reward, double predict_next_total_reward, double predict_total_reward_std);
    int GetID(){return ID_;};
    void SetID(int id){ ID_ =id;};
    double GetReward(){return current_reward_;};
    Eigen::VectorXd GetStateBegin(){return state_begin_;};
    Eigen::VectorXd GetStateEnd() {return state_end_;};
    Eigen::VectorXd GetAction() {return action_;};
    Eigen::VectorXd GetStateAction();

    double CombineReward();

    bool IsGoodExplore();
private:
    int ID_;
	double current_reward_;         // reward for taking current action
	double predict_total_reward_;   // predicted total reward for taking current action (Q value from critic network)
	double predict_total_reward_std_; // predicted total reward for taking the standard action (with no noise action)
    double predict_next_total_reward_;   // predicted total reward starting from next moment
	Eigen::VectorXd state_begin_;   // beginning state
	Eigen::VectorXd action_std_;    // the standard action for the beginning state (output from actor network)
	Eigen::VectorXd action_;        // the actual action, apply noise on the standard action
	Eigen::VectorXd state_end_;     // ending state
};


#endif // EXPTUPLE_H
