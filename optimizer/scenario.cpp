#include<iostream>
#include<cmath>
#include "optimizer/scenario.h"

int step_counter=0;

void simulationPreTickCallback (btDynamicsWorld *world, btScalar timeStep)
{
    Scenario* s = (Scenario*)world->getWorldUserInfo();
    s->UpdateSimulationStep(timeStep);

}

void Scenario::UpdateSimulationStep(double timeStep)
{
//    std::cout << "Step " << step_counter << ": "  << timeStep << std::endl;
    step_counter++;
    charac_->ApplyTorque(timeStep);
    charac_->UpdateDynamicState(timeStep);
}

Scenario::Scenario()
{

}

Scenario::Scenario(std::string arg_file)
{
    target_vel = Eigen::VectorXd::Zero(3);
    target_vel << 0.0, 0.0, 1.0; // default target velocity

    initPhysics();
    initTerrain();

    Json::Value args;
    JsonUtil::LoadFileIntoJsonValue(arg_file, args);
    initCharacter(args);
}

Scenario::~Scenario()
{
    exitPhysics();
}


void Scenario::initPhysics()
{

    //setCameraDistance(btScalar(50.));

    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();
    //m_collisionConfiguration->setConvexConvexMultipointIterations();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);

    m_broadphase = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);

    m_dynamicsWorld->setGravity(btVector3(0,-10,0));

    m_dynamicsWorld->setInternalTickCallback(simulationPreTickCallback,this,true);

}

void Scenario::initTerrain()
{
    btVector3 terrain_size(50.0, 50.0, 50.0);
    btVector3 terrain_pos(0,-50,0);
    terrain_ = new TerrainFlat(terrain_size, terrain_pos);
    btCollisionShape* terrainShape = terrain_->get_terrain_shape();
    m_collisionShapes.push_back(terrainShape);

    btRigidBody* terrainBody = terrain_->get_terrain_body();
    m_dynamicsWorld->addRigidBody(terrainBody);
}

void Scenario::initCharacter(const Json::Value& args)
{
    std::string skel_file_name = args["skeleton"].asString();
    charac_ = new Character(skel_file_name);

    //A kinematic controller is needed, no matter the character is kinematic or dynamic
    //If the character is dynamic, the motion is to initialize the posture from which the dynamic simulation starts
    charac_->SetKinematicFlag(true);
    Skeleton* skel = charac_->GetSkeleton();
    skel->EnableKinematics(m_dynamicsWorld);
    std::string motion_file_name = args["motion"].asString();
    charac_->LoadKinematic(motion_file_name);

    charac_->UpdateKinematicState(1.0/120.0);

    std::string actuator_file_name = args["actuator"].asString();
    std::string controller_file_name = args["control"].asString();

    assert(actuator_file_name!="");
    assert(controller_file_name!="");

    charac_->LoadActuatorSys(actuator_file_name);
    charac_->LoadController(controller_file_name);
    charac_->LoadSensorSys("");


    charac_->SetKinematicFlag(false);
    charac_->EnableDynamics(m_dynamicsWorld);
    charac_->LinkSkeletonWithActuator();
    //charac_->InitLocomotion();
}

void Scenario::initCharacterNet(NeuralNet* net)
{
    charac_->LoadNetwork(net);
}

void Scenario::Reset()
{
    charac_->SetKinematicFlag(true);
    charac_->ResetInitialPose();
}

bool Scenario::Evaluate(const Eigen::VectorXd action)
{
    charac_->SetKinematicFlag(false);
    Skeleton* skel = charac_->GetSkeleton();
    skel->EnableDynamics(m_dynamicsWorld);
    charac_->LinkSkeletonWithActuator();
    charac_->InitLocomotion();

    init_position = charac_->GetCOMPosition();
    double time_step = 1.0/120.0;
    double cycle_period = charac_->GetCycleDuration();
    double time_elapsed = 0.0;
    while(time_elapsed<cycle_period)
    {
        m_dynamicsWorld->stepSimulation(time_step);
        time_elapsed += time_step;
    }
    final_position = charac_->GetCOMPosition();
    bool done = IsTerminated();
    return done;
}

Eigen::VectorXd Scenario::Observe()
{
    // This is to get the state of the character, including posture
     Eigen::VectorXd state = charac_->IntegrateState();
    return state;
}

bool Scenario::IsTerminated()
{
    bool check = charac_->FallOff();
    return check;
}

double Scenario::CalcReward()
{
    double reward;

    Eigen::VectorXd trans = final_position-init_position;
    Eigen::VectorXd diff = trans - target_vel;
    double diff_norm = diff.squaredNorm();
    double omega = 0.5;
    reward = exp(-omega*diff_norm*diff_norm);

    return reward;
}


void Scenario::exitPhysics()
{
    //cleanup in the reverse order of creation/initialization

    //remove the rigidbodies from the dynamics world and delete them
    int i;
    for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ; i--)
    {
        btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }
        m_dynamicsWorld->removeCollisionObject( obj );
        delete obj;
    }

    //delete collision shapes
    for (int j=0; j<m_collisionShapes.size(); j++)
    {
        btCollisionShape* shape = m_collisionShapes[j];
        delete shape;
    }
    m_collisionShapes.clear();

    delete m_dynamicsWorld;

    delete m_solver;

    delete m_broadphase;

    delete m_dispatcher;

    delete m_collisionConfiguration;
}


