#include "optimizer/optimizerQLearning.h"
#include "utility/MathUtil.h"


void OptimizerQLearning::InitSetting()
{
    ind_iter_ = 0;
    max_iterations_ = 1000000;
    update_interval_ = 100;
    replay_buffer_max_size_ = 500000;
    mini_batch_size_ = 32;
}

OptimizerQLearning::OptimizerQLearning()
{
    InitSetting();
}

OptimizerQLearning::~OptimizerQLearning()
{

}

OptimizerQLearning::OptimizerQLearning(std::string arg_file)
{
    InitSetting();

    actor_ = new NeuralNet();
    critic_ = new NeuralNet();
    actor_target_ = new NeuralNet();
    critic_target_ = new NeuralNet();

    Json::Value args;
    JsonUtil::LoadFileIntoJsonValue(arg_file, args);
    InitNet(actor_, args["actor"].asString(), args["actor_solver"].asString());
    InitNet(critic_, args["critic"].asString(), args["critic_solver"].asString());
    InitNet(actor_target_, args["actor"].asString());
    InitNet(critic_target_, args["critic"].asString());
    InitTarget();
    InitReplayBuffer();
}


void OptimizerQLearning::InitNet(NeuralNet* net, std::string file_name)
{
    net->LoadNet(file_name);
    net->InitModel();
}

void OptimizerQLearning::InitNet(NeuralNet* net, std::string file_name, std::string solver_name)
{
    net->LoadNet(file_name);
    net->LoadSolver(solver_name);
    net->InitModel();
}



void OptimizerQLearning::InitTarget()
{
    critic_target_->CopyModelFrom(*critic_);
    actor_target_->CopyModelFrom(*actor_);
}

void OptimizerQLearning::InitReplayBuffer()
{

}

Eigen::VectorXd OptimizerQLearning::DecideAction(Eigen::VectorXd state)
{
    Eigen::VectorXd action = actor_->Evaluate(state);
    return action;
}

Eigen::VectorXd OptimizerQLearning::ExploreAction(Eigen::VectorXd state)
{
    Eigen::VectorXd action = actor_->Evaluate(state);
    Eigen::VectorXd noise = ActionNoise(action);
    Eigen::VectorXd action_with_noise = action + noise;
    return action;
}

Eigen::VectorXd OptimizerQLearning::ActionNoise(Eigen::VectorXd action)
{
    int num_dim_action = action.size();
    Eigen::VectorXd noise = Eigen::VectorXd::Random(num_dim_action);
    return noise;
}


void OptimizerQLearning::AppendExpTuple(ExpTuple exp)
{
    if(exp.IsGoodExplore())
    {
        int current_num_exptuples = replay_buffer_.size();
        exp.SetID(current_num_exptuples);
        replay_buffer_.push_back(exp);
    }
}


bool OptimizerQLearning::CheckEnoughReplay()
{
    int current_num_exptuples = replay_buffer_.size();
    if (current_num_exptuples>mini_batch_size_)
        return true;
    else
        return false;
}


void OptimizerQLearning::SampleMiniBatch(std::vector<ExpTuple>& mini_batch)
{
    MathUtil::random_unique(replay_buffer_.begin(), replay_buffer_.end(), mini_batch_size_);
    for(int inde=0; inde<mini_batch_size_; inde++)
        mini_batch.push_back(replay_buffer_[inde]);
}

void OptimizerQLearning::Update(std::vector<ExpTuple> mini_batch)
{
    UpdateCritic(mini_batch);
    UpdateActor(mini_batch);
    UpdateTarget();
    ind_iter_++;
}

void OptimizerQLearning::UpdateCritic(std::vector<ExpTuple> mini_batch)
{
    Problem prob;
    InitProb(mini_batch, prob, false);
    BuildProblemCritic(mini_batch, prob);
    critic_->Train(prob);
    critic_->SyncNetParams();
}


void OptimizerQLearning::UpdateActor(std::vector<ExpTuple> mini_batch)
{
    Problem prob;
    //std::vector<ExpTuple> filter_mini_batch = FilterActor(mini_batch);
    InitProb(mini_batch, prob, true);
    BuildProblemActor(mini_batch, prob);
    actor_->Train(prob);
    actor_->SyncNetParams();
}


void OptimizerQLearning::UpdateTarget()
{
    if ((ind_iter_%update_interval_) == 0)
    {
        critic_target_->CopyModelFrom(*critic_);
        actor_target_->CopyModelFrom(*actor_);
        actor_->OutputModel("output/model/actor_"+std::to_string(ind_iter_)+".h5");
    }
}

void OptimizerQLearning::Output()
{

}

bool OptimizerQLearning::CheckTermination()
{
    bool done = false;
    if(ind_iter_>max_iterations_)
        done = true;
    return done;
}

std::vector<ExpTuple> OptimizerQLearning::FilterActor(std::vector<ExpTuple>& mini_batch)
{
    std::vector<ExpTuple> new_mini_batch;
    for(unsigned int inde=0; inde<mini_batch.size(); inde++)
    {
        ExpTuple exp = mini_batch[inde];
        if(exp.IsGoodExplore())
            new_mini_batch.push_back(exp);
    }
    return new_mini_batch;
}

void OptimizerQLearning::InitProb(std::vector<ExpTuple> mini_batch, Problem& prob, bool is_actor)
{
    int x_size, y_size;
    if (is_actor)
    {
        x_size = actor_->GetInputSize();
        y_size = actor_->GetOutputSize();
    }
    else
    {
        x_size = critic_->GetInputSize();
        y_size = critic_->GetOutputSize();
    }
    int num_data = static_cast<int>(mini_batch.size());
	prob.mX.resize(num_data, x_size);
	prob.mY.resize(num_data, y_size);
}


void OptimizerQLearning::BuildProblem(std::vector<ExpTuple> mini_batch, Problem& out_prob)
{
    BuildProblemX(mini_batch, out_prob);
    BuildProblemY(mini_batch, out_prob);
}

void OptimizerQLearning::BuildProblemCritic(std::vector<ExpTuple> mini_batch, Problem& out_prob)
{
    int num_data = static_cast<int>(mini_batch.size());
	assert(out_prob.mX.rows() == num_data);
    for(int inde=0; inde<num_data; inde++)
    {
        ExpTuple exp = mini_batch[inde];
        out_prob.mX.row(inde) = exp.GetStateAction();
        Eigen::VectorXd reward(1);
        reward << exp.CombineReward();
        out_prob.mY.row(inde) = reward;
    }
}

void OptimizerQLearning::BuildProblemActor(std::vector<ExpTuple> mini_batch, Problem& out_prob)
{
	int num_data = static_cast<int>(mini_batch.size());
	assert(out_prob.mX.rows() == num_data);
    for(int inde=0; inde<num_data; inde++)
    {
        ExpTuple exp = mini_batch[inde];
        out_prob.mX.row(inde) = exp.GetStateBegin();
        out_prob.mY.row(inde) = exp.GetAction();
    }
}

void OptimizerQLearning::BuildProblemX(std::vector<ExpTuple> mini_batch, Problem& out_prob)
{
	int num_data = static_cast<int>(mini_batch.size());
//	assert(num_data == GetBatchSize());
	assert(out_prob.mX.rows() == num_data);

	for (int inde = 0; inde < num_data; ++inde)
    {

		ExpTuple et = mini_batch[inde];
		out_prob.mX.row(inde) = et.GetStateBegin();
	}
}

void OptimizerQLearning::BuildProblemY(std::vector<ExpTuple> mini_batch, Problem& out_prob)
{
	int num_data = static_cast<int>(mini_batch.size());
//	assert(num_data == GetBatchSize());
	assert(out_prob.mY.rows() == num_data);

	for (int inde = 0; inde < num_data; ++inde)
	{

		ExpTuple et = mini_batch[inde];

		//Eigen::VectorXd y;
		//BuildTupleY(net_id, tuple, y);
		out_prob.mY.row(inde) = et.GetAction();
	}
}

double OptimizerQLearning::PredictReward(Eigen::VectorXd state, Eigen::VectorXd action)
{
    Eigen::VectorXd state_n_action = MathUtil::ConcatVectors(state, action);
    Eigen::VectorXd critic_output = critic_target_->Evaluate(state_n_action);
    return critic_output[0];
}




