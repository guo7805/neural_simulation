#include "optimizer/expTuple.h"
#include "utility/MathUtil.h"


ExpTuple::ExpTuple()
{

}

ExpTuple::ExpTuple(Eigen::VectorXd stateBeg, Eigen::VectorXd action_std, Eigen::VectorXd action, Eigen::VectorXd stateEnd,
                    double reward, double predict_total_reward, double predict_next_total_reward, double predict_total_reward_std)
{
	current_reward_ = reward;
	predict_total_reward_ = predict_total_reward;
	predict_total_reward_std_ = predict_total_reward_std;
    predict_next_total_reward_ = predict_next_total_reward;

	state_begin_ = stateBeg;
	action_std_ = action_std;
	action_ = action;
	state_end_ = stateEnd;
}


bool ExpTuple::IsGoodExplore()
{
    double predict_total_reward_noise = CombineReward();
    // This confirms that the actor exploration achieves better reward than choosing the standard action
    if (predict_total_reward_noise > predict_total_reward_std_)
        return true;
    else
        return false;
}

Eigen::VectorXd ExpTuple::GetStateAction()
{
    return MathUtil::ConcatVectors(state_begin_, action_);
}

double ExpTuple::CombineReward()
{
    return current_reward_ + predict_next_total_reward_;
}

