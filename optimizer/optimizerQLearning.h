#ifndef OPTIMIZERQLEARNING_H
#define OPTIMIZERQLEARNING_H


#include "learning/neuralNet.h"
#include "optimizer/expTuple.h"
#include "utility/JsonUtil.h"

// For explanation on parameters, refer to the paper from Lillicrap, et al. Continuous Control with Deep Reinforcement Learning
const int GAMMA = 0.9;
const int TAU = 0.001;



class OptimizerQLearning
{
public:
    OptimizerQLearning();
    ~OptimizerQLearning();
    OptimizerQLearning(std::string arg_file);
    // initialization stage
    void InitNet(NeuralNet* net, std::string file_name);
    void InitNet(NeuralNet* net, std::string file_name, std::string solver_name);
    void InitTarget();
    void InitReplayBuffer();

    void InitSetting();

    NeuralNet* GetActor(){return actor_;};

    // simulation stage
    Eigen::VectorXd DecideAction(Eigen::VectorXd state);
    Eigen::VectorXd ExploreAction(Eigen::VectorXd state);
    Eigen::VectorXd ActionNoise(Eigen::VectorXd action);
    double PredictReward(Eigen::VectorXd state, Eigen::VectorXd action);
    void AppendExpTuple(ExpTuple exp);

    // update stage
    bool CheckEnoughReplay();
    void SampleMiniBatch(std::vector<ExpTuple>& mini_batch);
    void Update(std::vector<ExpTuple> mini_batch);
    void UpdateCritic(std::vector<ExpTuple> mini_batch);
    void UpdateActor(std::vector<ExpTuple> mini_batch);
    void UpdateTarget();

    std::vector<ExpTuple> FilterActor(std::vector<ExpTuple>& mini_batch);
    void InitProb(std::vector<ExpTuple> mini_batch, Problem& prob, bool is_actor);
    void BuildProblem(std::vector<ExpTuple> mini_batch, Problem& out_prob);
    void BuildProblemCritic(std::vector<ExpTuple> mini_batch, Problem& out_prob);
    void BuildProblemActor(std::vector<ExpTuple> mini_batch, Problem& out_prob);
    void BuildProblemX(std::vector<ExpTuple> mini_batch, Problem& out_prob);
    void BuildProblemY(std::vector<ExpTuple> mini_batch, Problem& out_prob);

    // utility
    bool CheckTermination();
    void Output();
private:
    int ind_iter_;
    int max_iterations_;
    int update_interval_;
    int replay_buffer_max_size_;
    int mini_batch_size_;
    NeuralNet* critic_;
    NeuralNet* actor_;
    NeuralNet* critic_target_;
    NeuralNet* actor_target_;
    std::vector<ExpTuple> replay_buffer_;
};


#endif // OPTIMIZERQLEARNING_H
